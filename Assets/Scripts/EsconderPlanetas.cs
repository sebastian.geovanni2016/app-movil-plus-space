﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EsconderPlanetas : MonoBehaviour
{
    public SistemaBotones Esconder;
    public void EsconderElemento(){
        Esconder=GameObject.Find("Escondiendo").GetComponent<SistemaBotones>();
        Esconder.play.SetActive(false);
    }
}
