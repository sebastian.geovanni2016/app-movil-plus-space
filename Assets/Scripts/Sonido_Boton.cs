﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Sonido_Boton : MonoBehaviour
{
    public AudioClip clip;
    public AudioSource fuenteAudio {get{return GetComponent<AudioSource>();}}
    public Button boton {get{return GetComponent<Button>();}}
    void Start()
    {
        gameObject.AddComponent<AudioSource>();
        boton.onClick.AddListener (PlaySound);

    }

    // Update is called once per frame
    void PlaySound()
    {  
       fuenteAudio.PlayOneShot (clip); 
    }
}
