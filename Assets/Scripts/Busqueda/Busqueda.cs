﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Busqueda : MonoBehaviour
{
    // Start is called before the first frame update
    public string[] preguntas;
    public int n_preguntas; 
    public int cont_preguntas=0;
    public GameObject Panel;
    public int n_pregunta;
    public Text preguntaGO;
    public GameObject correcta;
    public GameObject incorrecta;
    public GameObject Skybox;
    private float x;
    private float y;
    private float z;
    public bool correcto;
    public Conexiones conexiones;

public void cambiaPregunta(){
        if (cont_preguntas==n_preguntas-1)
        {
            Panel.SetActive(true);
        }
        else{
        n_pregunta = Random.Range(0,preguntas.Length);
        preguntaGO.text = preguntas[n_pregunta];
        cont_preguntas++;
        }    
    }

public void Correcta(){
    correcta.SetActive(false);
    correcto=true;
    cambiaPregunta();
}
public void Incorrecta(){
    incorrecta.SetActive(false);
    correcto=false;
    cambiaPregunta();
}
public void Planeta(){
if (n_pregunta==0)
{
    correcta.SetActive(true);
    Invoke( "Correcta", 3f);
            //objeto.SetActive(false);
    conexiones.correctaPlaneta();
}
else
{
    incorrecta.SetActive(true);
    Invoke( "Incorrecta", 3f);
    conexiones.incorrectaPlaneta();

}
}
public void PlanetaConSatelite(){
if (n_pregunta==1)
{
    correcta.SetActive(true);
    Invoke( "Correcta", 3f);
    conexiones.correctaPlanetaS();
}
else
{
    incorrecta.SetActive(true);
    Invoke( "Incorrecta", 3f);
    conexiones.incorrectaPlanetaS();
}
}
public void PlanetaConAnillo(){
if (n_pregunta==2)
{
    correcta.SetActive(true);
    Invoke( "Correcta", 3f);
    conexiones.correctaPlanetaA();
    
}
else
{
    incorrecta.SetActive(true);
    Invoke( "Incorrecta", 3f);
    conexiones.incorrectaPlanetaA();
}
}
public void Estrella(){
if (n_pregunta==3)
{
    correcta.SetActive(true);
    Invoke( "Correcta", 3f);
    conexiones.correctaEstrella();
}
else
{
    incorrecta.SetActive(true);
    Invoke( "Incorrecta", 3f);
    conexiones.incorrectaEstrella();
}
}
public void Supernova(){
if (n_pregunta==4)
{
    correcta.SetActive(true);
    Invoke( "Correcta", 3f);
    conexiones.correctaSuper();
}
else
{
    incorrecta.SetActive(true);
    Invoke( "Incorrecta", 3f);
    conexiones.incorrectaSuper();
}
}
public void Nebulosa(){
if (n_pregunta==5)
{
    correcta.SetActive(true);
    Invoke( "Correcta", 3f);
    conexiones.correctaNebu();
}
else
{
    incorrecta.SetActive(true);
    Invoke( "Incorrecta", 3f);
    conexiones.incorrectaNebu();
}
}
public void Galaxia(){
if (n_pregunta==12)
{
    correcta.SetActive(true);
    Invoke( "Correcta", 3f);
    conexiones.correctaGala();
}
else
{
    incorrecta.SetActive(true);
    Invoke( "Incorrecta", 3f);
    conexiones.incorrectaGala();

}
}
public void EstrellaRoja(){
if (n_pregunta==6||n_pregunta==3)
{
    correcta.SetActive(true);
    Invoke( "Correcta", 3f);
    conexiones.correctaEstrella();
}
else
{
    incorrecta.SetActive(true);
    Invoke( "Incorrecta", 3f);
    conexiones.incorrectaEstrella();
}
}
public void Planet_rosa(){
if (n_pregunta==7||n_pregunta==0)
{
    correcta.SetActive(true);
    Invoke( "Correcta", 3f);
    conexiones.correctaPlaneta();
}
else
{
    incorrecta.SetActive(true);
    Invoke( "Incorrecta", 3f);
    conexiones.incorrectaPlaneta();
}
}
public void Planet_tierra(){
if (n_pregunta==8||n_pregunta==0||n_pregunta==1)
{
    correcta.SetActive(true);
    Invoke( "Correcta", 3f);
    conexiones.correctaPlaneta();
}
else
{
    incorrecta.SetActive(true);
    Invoke( "Incorrecta", 3f);
    conexiones.incorrectaPlaneta();
}
}
public void Supernova_celeste(){
if (n_pregunta==9||n_pregunta==4)
{
    correcta.SetActive(true);
    Invoke( "Correcta", 3f);
    conexiones.correctaSuper();
}
else
{
    incorrecta.SetActive(true);
    Invoke( "Incorrecta", 3f);
    conexiones.incorrectaSuper();
}
}
public void Galaxia_Azul(){
if (n_pregunta==10||n_pregunta==12)
{
    correcta.SetActive(true);
    Invoke( "Correcta", 3f);
    conexiones.correctaGala();
}
else
{
    incorrecta.SetActive(true);
    Invoke( "Incorrecta", 3f);
    conexiones.incorrectaGala();
}
}
public void planeta_rojo(){
if (n_pregunta==11||n_pregunta==0)
{
    correcta.SetActive(true);
    Invoke( "Correcta", 3f);
    conexiones.correctaPlaneta();
}
else
{
    incorrecta.SetActive(true);
    Invoke( "Incorrecta", 3f);
    conexiones.incorrectaPlaneta();
}
}
    void Start()
    {
        x=Random.Range(0,360);
        y=Random.Range(0,360);
        z=Random.Range(0,360);
        cambiaPregunta(); 
        correcta.SetActive(false);
        incorrecta.SetActive(false);
        Panel.SetActive(false);
        Skybox.transform.Rotate(x,y,z);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
