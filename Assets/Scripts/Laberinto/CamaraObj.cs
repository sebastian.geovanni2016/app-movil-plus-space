﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamaraObj : MonoBehaviour
{
    public GameObject target;
    public GameObject marte;
    public GameObject neptuno;
    public GameObject mercurio;
    public GameObject venus;
    public GameObject urano;
    public GameObject saturno;
    public GameObject jupi;
    public Colocar colocar;
    public Vector3 mover;


    // Update is called once per frame
    void Update()
    {
        //mover = new Vector3 (target.gameObject.transform.position.x, target.gameObject.transform.position.y + 10f,target.gameObject.transform.position.z );
        if (colocar.tierraactivo == true)
        {
            mover = new Vector3(target.gameObject.transform.position.x, target.gameObject.transform.position.y + 15f, target.gameObject.transform.position.z);
            this.transform.position = mover;
        }
        if (colocar.marteactivo == true)
        {
            mover = new Vector3(marte.gameObject.transform.position.x, marte.gameObject.transform.position.y + 10f, marte.gameObject.transform.position.z);
            this.transform.position = mover;
        }
        if (colocar.neptunoactivo == true)
        {
            mover = new Vector3(neptuno.gameObject.transform.position.x, neptuno.gameObject.transform.position.y + 10f, neptuno.gameObject.transform.position.z);
            this.transform.position = mover;
        }
        if (colocar.mercurioactivo == true)
        {
            mover = new Vector3(mercurio.gameObject.transform.position.x, mercurio.gameObject.transform.position.y + 10f, mercurio.gameObject.transform.position.z);
            this.transform.position = mover;
        }
        if (colocar.venusactivo == true)
        {
            mover = new Vector3(venus.gameObject.transform.position.x, venus.gameObject.transform.position.y + 10f, venus.gameObject.transform.position.z);
            this.transform.position = mover;
        }
        if (colocar.uranoactivo == true)
        {
            mover = new Vector3(urano.gameObject.transform.position.x, urano.gameObject.transform.position.y + 10f, urano.gameObject.transform.position.z);
            this.transform.position = mover;
        }
        if (colocar.saturnoactivo == true)
        {
            mover = new Vector3(saturno.gameObject.transform.position.x, saturno.gameObject.transform.position.y + 10f, saturno.gameObject.transform.position.z);
            this.transform.position = mover;
        }
        if (colocar.jupiteractivo == true)
        {
            mover = new Vector3(jupi.gameObject.transform.position.x, jupi.gameObject.transform.position.y + 10f, jupi.gameObject.transform.position.z);
            this.transform.position = mover;
        }
    }
}
