﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoverR : MonoBehaviour
{
    public float speed = 10.0F;
    public float rotationSpeed = 50.0F;
    public Transform myTransform;

    void Start()
    {
        myTransform = transform;
    }
    void Update()
    {
        if (Input.GetKey(KeyCode.UpArrow))
        {
            myTransform.position += myTransform.forward * speed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            myTransform.position -= myTransform.forward * speed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            myTransform.eulerAngles -= new Vector3(0, rotationSpeed * Time.deltaTime, 0);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            myTransform.eulerAngles += new Vector3(0, rotationSpeed * Time.deltaTime, 0);
        }

        /*if (Input.GetKeyDown(KeyCode.Space))
        {
            gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(0,500,0));
        }*/
    }
    public void OnTriggerEnter(Collider other)
    {
        Debug.Log("colision");
    }
}
