﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class desactivar_botones : MonoBehaviour
{
    public Text correcto;
    public Text incorrecto;
    // Start is called before the first frame update
    void Start()
    {
        correcto.gameObject.SetActive(false);
        incorrecto.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
