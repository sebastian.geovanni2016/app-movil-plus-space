﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoverPlaneta : MonoBehaviour
{
    Rigidbody rb;
    public float velocidad;
    public Transform myTransform;
    public float rotationSpeed = 50.0F;
    public float speed = 10.0F;

    void Start()
    {
        myTransform = transform;
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.UpArrow))
        {
            myTransform.position += myTransform.forward * speed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            myTransform.position -= myTransform.forward * speed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            myTransform.eulerAngles -= new Vector3(0, rotationSpeed * Time.deltaTime, 0);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            myTransform.eulerAngles += new Vector3(0, rotationSpeed * Time.deltaTime, 0);
        }

        /*if (Input.GetKeyDown(KeyCode.Space))
        {
            gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(0,500,0));
        }*/
    }

    public void OnTriggerEnter(Collider other)
    {
        Destroy(other.gameObject);
    }

    public void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    public void FixedUpdate()
    {
        float movimientoHorizontal = Input.GetAxis("Horizontal");
        float movimientoVertical = Input.GetAxis("Vertical");
        Vector3 movimiento = new Vector3(movimientoHorizontal, 0, movimientoVertical);
        rb.AddForce(movimiento * velocidad);
    }
}
