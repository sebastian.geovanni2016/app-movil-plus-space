﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlechaTierra : MonoBehaviour
{
    public float speed;

    private Rigidbody rb;
    public static bool activo = false;
    


    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }
    public void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);
    }

    
    public void OnTriggerEnter(Collider other)
    {
        
        if (other.gameObject.CompareTag("p_tierra"))
        {
            
            speed = 0;
            rb.Sleep();
        }

    }
}


