﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Accelerometro1 : MonoBehaviour
{
    // gravity constant
    public float g = 9.8f;

    void FixedUpdate()
    {
        // normalize axis
        var gravity = new Vector3(
            -Input.acceleration.x,
            Input.acceleration.y,
            Input.acceleration.z
        ) * g * -1;

        GetComponent<Rigidbody>().AddForce(gravity, ForceMode.Acceleration);
    }
}
