﻿using UnityEngine;

public class Colocar : MonoBehaviour
{
    public GameObject neptuno;
    public GameObject tierra;
    public GameObject marte;
    public GameObject venus;
    public GameObject jupiter;
    public GameObject saturno;
    public GameObject mercurio;
    public GameObject urano;
    public FlechaTierra camtierra;
    public GameObject b_neptuno;
    public GameObject b_tierra;
    public GameObject b_marte;
    public GameObject b_venus;
    public GameObject b_jupiter;
    public GameObject b_saturno;
    public GameObject b_mercurio;
    public GameObject b_urano;
    //public bool activo=false;
    public bool tierraactivo = false;
    public bool marteactivo = false;
    public bool venusactivo = false;
    public bool neptunoactivo = false;
    public bool jupiteractivo = false;
    public bool saturnoactivo = false;
    public bool uranoactivo = false;
    public bool mercurioactivo = false;
    
    

    public void colocarNeptuno()
    {
        
        if (tierraactivo == false && marteactivo == false && venusactivo == false && neptunoactivo == false && jupiteractivo == false && saturnoactivo == false && uranoactivo == false && mercurioactivo == false)
        {            
             neptuno.SetActive(true);
             neptunoactivo = true;
             b_neptuno.SetActive(false);
            
            //activo = true;
        }
        //neptuno.transform.localPosition = new Vector3(0, 0, 0);
    }
    public void colocarTierra()
    {
        if (tierraactivo == false && marteactivo ==false && venusactivo == false && neptunoactivo == false && jupiteractivo == false && saturnoactivo == false && uranoactivo == false && mercurioactivo == false)
        {
            tierra.SetActive(true);
            tierraactivo = true;
            b_tierra.SetActive(false);
            //activo = true;
        }
    }
    public void colocarMarte()
    {
        if (tierraactivo == false && marteactivo == false && venusactivo == false && neptunoactivo == false && jupiteractivo == false && saturnoactivo == false && uranoactivo == false && mercurioactivo == false)
        {
            marte.SetActive(true);
            marteactivo = true;
            b_marte.SetActive(false);
            //activo = true;
            //marte.transform.localPosition = new Vector3(0, 0, 0);
        }
    }
    public void colocarVenus()
    {
        if (tierraactivo == false && marteactivo == false && venusactivo == false && neptunoactivo == false && jupiteractivo == false && saturnoactivo == false && uranoactivo == false && mercurioactivo == false)
        {
            venus.SetActive(true);
            venusactivo = true;
            b_venus.SetActive(false);
            //activo = true;
            //venus.transform.localPosition = new Vector3(0, 0, 0);
        }
    }
    public void colocarJupiter()
    {
        if (tierraactivo == false && marteactivo == false && venusactivo == false && neptunoactivo == false && jupiteractivo == false && saturnoactivo == false && uranoactivo == false && mercurioactivo == false)
        {
            jupiter.SetActive(true);
            jupiteractivo = true;
            b_jupiter.SetActive(false);
            //activo = true;
            //jupiter.transform.localPosition = new Vector3(0, 0, 0);
        }
    }
    public void colocarSaturno()
    {
        if (tierraactivo == false && marteactivo == false && venusactivo == false && neptunoactivo == false && jupiteractivo == false && saturnoactivo == false && uranoactivo == false && mercurioactivo == false)
        {
            saturno.SetActive(true);
            saturnoactivo = true;
            b_saturno.SetActive(false);
            //activo = true;
            //saturno.transform.localPosition = new Vector3(0, 0, 0);
        }
    }
    public void colocarMercurio()
    {
        if (tierraactivo == false && marteactivo == false && venusactivo == false && neptunoactivo == false && jupiteractivo == false && saturnoactivo == false && uranoactivo == false && mercurioactivo == false)
        {
            mercurio.SetActive(true);
            mercurioactivo = true;
            b_mercurio.SetActive(false);
            //activo = true;
            //mercurio.transform.localPosition = new Vector3(0, 0, 0);
        }
    }
    public void colocarUrano()
    {
        if (tierraactivo == false && marteactivo == false && venusactivo == false && neptunoactivo == false && jupiteractivo == false && saturnoactivo == false && uranoactivo == false && mercurioactivo == false)
        {
            urano.SetActive(true);
            uranoactivo = true;
            b_urano.SetActive(false);
            //activo = true;
            //urano.transform.localPosition = new Vector3(0, 0, 0);
        }
    }
}
