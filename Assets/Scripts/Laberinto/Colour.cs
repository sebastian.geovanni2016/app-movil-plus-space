﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Colour : MonoBehaviour
{
    
    public Color Green;
    //public Text correcto;
    //public Text incorrecto;
    public int contador=0;
    public int seconds=1;
    public GameObject correcto;
    public GameObject incorrecto;
    public GameObject final;
    public int contadortierra = 0;

    void Start()
    {
        StartCoroutine(contadorfunc());
    }

    void Update()
    {
        contadortierra = GameObject.Find("p_tierra").GetComponent<Colour>().contador + GameObject.Find("p_mercurio").GetComponent<Colour>().contador + GameObject.Find("p_jupiter").GetComponent<Colour>().contador + GameObject.Find("p_urano").GetComponent<Colour>().contador + GameObject.Find("p_marte").GetComponent<Colour>().contador + GameObject.Find("p_saturno").GetComponent<Colour>().contador + GameObject.Find("p_venus").GetComponent<Colour>().contador + GameObject.Find("p_neptuno").GetComponent<Colour>().contador;
        if (contadortierra == 8)
        {
            Finish();
            //Finish();
        }
    }

    public void Awake()
    {
        
    }
   
    void OnTriggerEnter(Collider other)
    {
        //int n = 0;
        //while (n < 8)
        //{
        

        
            if (other.gameObject.tag == "tierra")
            {
                if (this.gameObject.name == "p_tierra")
                {
                    transform.GetComponent<Renderer>().material.color = Green;
                    //print("b");
                    //correcto.gameObject.SetActive(true);
                    IsCorrect();
                    //print("c");
                    //Debug.Log("1");
                    contador++;

                }
                if (this.gameObject.name != "p_tierra")
                {
                    IsIncorrect();
                    //Debug.Log("colision");
                    //incorrecto.gameObject.SetActive(true);
                    //Debug.Log("1");
                    
                }
            }

            if (other.gameObject.tag == "mercurio")
            {
                if (this.gameObject.name == "p_mercurio")
                {
                    transform.GetComponent<Renderer>().material.color = Green;
                    IsCorrect();
                    contador++;
                }
                if (this.gameObject.name != "p_mercurio")
                {
                    IsIncorrect();
                    //Debug.Log("colision");
                    //incorrecto.gameObject.SetActive(true);
                    //Debug.Log("1");
                    
                }
            
            }
        
            if (other.gameObject.tag == "venus")
            {
                if (this.gameObject.name == "p_venus")
                {
                    transform.GetComponent<Renderer>().material.color = Green;
                    IsCorrect();
                    contador++;
                }
                if (this.gameObject.name != "p_venus")
                {
                    IsIncorrect();
                    //Debug.Log("colision");
                    //incorrecto.gameObject.SetActive(true);
                    //Debug.Log("1");

                }
            }
            if (other.gameObject.tag == "jupiter")
            {
                if (this.gameObject.name == "p_jupiter")
                {
                    transform.GetComponent<Renderer>().material.color = Green;
                    IsCorrect();
                    contador++;
                }
                if (this.gameObject.name != "p_jupiter")
                {
                    IsIncorrect();
                    //Debug.Log("colision");
                    //incorrecto.gameObject.SetActive(true);
                    //Debug.Log("1");

                }
            }
            if (other.gameObject.tag == "marte")
            {
                if (this.gameObject.name == "p_marte")
                {
                    transform.GetComponent<Renderer>().material.color = Green;
                    IsCorrect();
                    contador++;
                }
                if (this.gameObject.name != "p_marte")
                {
                    IsIncorrect();
                    //Debug.Log("colision");
                    //incorrecto.gameObject.SetActive(true);
                    //Debug.Log("1");

                }
            }
            if (other.gameObject.tag == "neptuno")
            {
                if (this.gameObject.name == "p_neptuno")
                {
                    transform.GetComponent<Renderer>().material.color = Green;
                    IsCorrect();
                    contador++;
                }
                if (this.gameObject.name != "p_neptuno")
                {
                    IsIncorrect();
                    //Debug.Log("colision");
                    //incorrecto.gameObject.SetActive(true);
                    //Debug.Log("1");

                }
            }
            if (other.gameObject.tag == "urano")
            {
                if (this.gameObject.name == "p_urano")
                {
                    transform.GetComponent<Renderer>().material.color = Green;
                    IsCorrect();
                    contador++;
                }
                if (this.gameObject.name != "p_urano")
                {
                    IsIncorrect();
                    //Debug.Log("colision");
                    //incorrecto.gameObject.SetActive(true);
                    //Debug.Log("1");

                }
            }
            if (other.gameObject.tag == "saturno")
            {
                if (this.gameObject.name == "p_saturno")
                {
                    transform.GetComponent<Renderer>().material.color = Green;
                    IsCorrect();
                    contador++;
                }
                if (this.gameObject.name != "p_saturno")
                {
                    IsIncorrect();
                    //Debug.Log("colision");
                    //incorrecto.gameObject.SetActive(true);
                    //Debug.Log("1");

                }
            }


        //}


    }
    public void IsCorrect()
    {
        //print("a");
        correcto.gameObject.SetActive(true);
        //print("a2");
        Invoke("DisCorrect", 4);
        //correcto.gameObject.SetActive(false);

    }

    public void DisCorrect()
    {
        correcto.gameObject.SetActive(false);
    }

    public void IsIncorrect()
    {
        incorrecto.gameObject.SetActive(true);
        Invoke("DisIncorrecto", 4);
        //StartCoroutine(contadorfunc());
        //incorrecto.gameObject.SetActive(false);
    }

    public void DisIncorrecto()
    {
        incorrecto.gameObject.SetActive(false);
    }

    IEnumerator contadorfunc()
    {
        if (contador == 1)
        {
            IsCorrect();
            yield return new WaitForSeconds(seconds);
        }
    }

    public void Finish()
    {
        final.gameObject.SetActive(true);
        
    }

    public void DisFinish()
    {
        final.gameObject.SetActive(false);
    }

    

}
