﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FlechaVenus : MonoBehaviour
{
    public float g = 9.8f;
    private Rigidbody rb;
    public Colocar colocar;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }


    void FixedUpdate()
    {
        // normalize axis
        var gravity = new Vector3(
            Input.acceleration.x,
            Input.acceleration.z,
            Input.acceleration.y
        ) * g;

        rb.AddForce(gravity, ForceMode.Acceleration);
    }
    public void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.CompareTag("p_venus"))
        {
            //rb.Sleep();
            colocar.venusactivo = false;
            rb.isKinematic = true;
            rb.velocity = Vector3.zero;
            GameObject.Find("Conexiones").GetComponent<Conexiones>().ColisionTCorrecto(SceneManager.GetActiveScene().name, other.gameObject.name);
        }
        else if (other.gameObject.CompareTag("p_jupiter") || other.gameObject.CompareTag("p_tierra") || other.gameObject.CompareTag("p_urano") || other.gameObject.CompareTag("p_mercurio") || other.gameObject.CompareTag("p_marte") || other.gameObject.CompareTag("p_neptuno") || other.gameObject.CompareTag("p_saturno"))
        {
            GameObject.Find("Conexiones").GetComponent<Conexiones>().ColisionTIncorrecto(SceneManager.GetActiveScene().name, other.gameObject.name);
        }

    }
}
