﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SistemaBotones : MonoBehaviour
{
    //bbdd
    public Conexiones conexiones;
    //elemento-----------------------------------------
    [SerializeField] private GameObject[] elemento;
    [SerializeField] private GameObject[] elemento1;
    public int pos_elm = 0;
    public GameObject play;
    public GameObject play1;
    [SerializeField] private Text n_elemento;
    public string[] nom_elemento;
    //tamaño
    public int pos_tam = 0;
    public float[] tam;
    public float[] posx;
    public float[] posgx;
    [SerializeField] private Text n_tam;
    public string[] nom_tam;
    //color--------------------------------------------
    public int pos_color = 0;
    public Material[] material_p;
    public Material[] material_e;
    public Texture[] material_s;
    public Texture[] material_n;
    public Material[] material_g;
    public Text n_color;
    public string[] nom_color_planeta;
    public string[] nom_color_estrella;
    public string[] nom_color_supernova_nebulosa;
    public string[] nom_color_galaxia;
    //bloqueo
    [SerializeField] private GameObject bloqueo;
    public recompensa recompensaC;
    /* 
	[SerializeField]private bool[] desbloqueo_p;
	[SerializeField]private bool[] desbloqueo_e;
	[SerializeField]private bool[] desbloqueo_s;
	[SerializeField]private bool[] desbloqueo_n;
	[SerializeField]private bool[] desbloqueo_g;*/
    //Aceptar------------------------------------------------------------------------------

    public int cont = 0;
    public int numCreado;
    private int maximo = 0;
    [SerializeField] private GameObject botonAceptar;
    //public Recompensa;

    //recompensa

    //Cambiar elemento---------------------------------------------------------------------
    public void ActivarAnterior()
    {
        Renderer rend = play.GetComponent<Renderer>();
        Renderer rend1 = play1.GetComponent<Renderer>();
        pos_elm--;
        pos_color = 0;
        pos_tam = 0;
        Destroy(play);
        Destroy(play1);
        bloqueo.SetActive(false);
        botonAceptar.SetActive(true);
        if (pos_elm < 0)
        {
            pos_elm = elemento.Length - 1;
        }
        if (pos_elm < elemento.Length && pos_elm >= 0)
        {
            play = Instantiate(elemento[pos_elm]) as GameObject;
            play1 = Instantiate(elemento1[pos_elm]) as GameObject;
            n_elemento.text = nom_elemento[pos_elm];
            n_tam.text = nom_tam[pos_tam];
            if (pos_elm == 0 || pos_elm == 1 || pos_elm == 2)
            {
                n_color.text = nom_color_planeta[pos_color];
            }
            else if (pos_elm == 3)
            {
                n_color.text = nom_color_estrella[pos_color];
            }
            else if (pos_elm == 4 || pos_color == 5)
            {
                n_color.text = nom_color_supernova_nebulosa[pos_color];
                rend.sharedMaterial.mainTexture = material_s[0];
                rend1.sharedMaterial.mainTexture = material_s[0];
                n_color.text = nom_color_supernova_nebulosa[pos_color];
            }
            else if (pos_elm == 6)
            {
                n_color.text = nom_color_galaxia[pos_color];
            }
            //n_color.text =nom_color[pos_color];
        }
    }
    public void ActivarSiguiente()
    {
        Renderer rend = play.GetComponent<Renderer>();
        Renderer rend1 = play1.GetComponent<Renderer>();
        pos_elm++;
        pos_color = 0;
        pos_tam = 0;
        Destroy(play);
        Destroy(play1);
        bloqueo.SetActive(false);
        botonAceptar.SetActive(true);
        if (pos_elm >= elemento.Length)
        {
            pos_elm = 0;
        }
        if (pos_elm < elemento.Length && pos_elm >= 0)
        {
            play = Instantiate(elemento[pos_elm]) as GameObject;
            play1 = Instantiate(elemento1[pos_elm]) as GameObject;
            n_elemento.text = nom_elemento[pos_elm];
            n_tam.text = nom_tam[pos_tam];
            if (pos_elm == 0 || pos_elm == 1 || pos_elm == 2)
            {
                n_color.text = nom_color_planeta[pos_color];
            }
            else if (pos_elm == 3)
            {
                n_color.text = nom_color_estrella[pos_color];
            }
            else if (pos_elm == 4 || pos_color == 5)
            {
                n_color.text = nom_color_supernova_nebulosa[pos_color];
                rend.sharedMaterial.mainTexture = material_s[0];
                rend1.sharedMaterial.mainTexture = material_s[0];
                n_color.text = nom_color_supernova_nebulosa[pos_color];
            }
            else if (pos_elm == 6)
            {
                n_color.text = nom_color_galaxia[pos_color];
            }
            //n_color.text =nom_color[pos_color];
        }
    }
    //Tamaño----------------------------------------------------------------
     //Tamaño----------------------------------------------------------------
    public void TamAnterior()
    {
        pos_tam--;
        bloqueo.SetActive(false);
        botonAceptar.SetActive(true);
        //planetas
        if (pos_elm == 0 || pos_elm == 1 || pos_elm == 2)
        {
            if (pos_tam < 0)
            {
                pos_tam = tam.Length-1;
            }
            if (pos_tam < tam.Length && pos_tam >= 0)
            {
                play.transform.localScale = new Vector3(tam[pos_tam], tam[pos_tam], tam[pos_tam]);
                play1.transform.localScale = new Vector3(tam[pos_tam], tam[pos_tam], tam[pos_tam]);
                n_tam.text = nom_tam[pos_tam];
                n_color.text = nom_color_planeta[pos_color];
            }
            //candado planetas
            if (recompensaC.desbloqueo_p[pos_color] == false)
            {
                bloqueo.SetActive(true);
                botonAceptar.SetActive(false);

            }
            else
            {
                bloqueo.SetActive(false);
                botonAceptar.SetActive(true);
            }
        }
        //estrellas y supernovas
        if (pos_elm == 3 || pos_elm == 4)
        {
            if (pos_tam < 0)
            {
                pos_tam = tam.Length-1;
            }
            if (pos_tam < tam.Length && pos_tam >= 0)
            {
                play.transform.localScale = new Vector3(posx[pos_tam], posx[pos_tam], posx[pos_tam]);
                play1.transform.localScale = new Vector3(posx[pos_tam], posx[pos_tam], posx[pos_tam]);
                n_tam.text = nom_tam[pos_tam];
                if (pos_elm == 3)
                {
                    n_color.text = nom_color_estrella[pos_color];
                }
                else if (pos_elm == 4)
                {
                    n_color.text = nom_color_supernova_nebulosa[pos_color];
                }

            }
            //candado estrellas
            if (recompensaC.desbloqueo_e[pos_color] == false)
            {
                bloqueo.SetActive(true);
                botonAceptar.SetActive(false);
            }
            else
            {
                bloqueo.SetActive(false);
                botonAceptar.SetActive(true);
            }
            //candado supernova
            if (recompensaC.desbloqueo_s[pos_color] == false)
            {
                bloqueo.SetActive(true);
                botonAceptar.SetActive(false);
            }
            else
            {
                bloqueo.SetActive(false);
                botonAceptar.SetActive(true);
            }
        }
        //nebulosas, galaxias
        if (pos_elm == 5 || pos_elm == 6)
        {
            if (pos_tam < 0)
            {
                pos_tam = tam.Length-1;
            }
            if (pos_tam < tam.Length && pos_tam >= 0)
            {
                play.transform.localScale = new Vector3(posgx[pos_tam], posgx[pos_tam], posgx[pos_tam]);
                play1.transform.localScale = new Vector3(posgx[pos_tam], posgx[pos_tam], posgx[pos_tam]);
                n_tam.text = nom_tam[pos_tam];
                if (pos_elm == 5)
                {
                    n_color.text = nom_color_supernova_nebulosa[pos_color];
                }
                else if (pos_elm == 6)
                {
                    n_color.text = nom_color_galaxia[pos_color];
                }
                //n_color.text =nom_color[pos_color];
            }
            //candado nebulosa
            if (recompensaC.desbloqueo_n[pos_color] == false)
            {
                bloqueo.SetActive(true);
                botonAceptar.SetActive(false);
            }
            else
            {
                bloqueo.SetActive(false);
                botonAceptar.SetActive(true);
            }
            //candado galaxia
            if (recompensaC.desbloqueo_g[pos_color] == false)
            {
                bloqueo.SetActive(true);
                botonAceptar.SetActive(false);
            }
            else
            {
                bloqueo.SetActive(false);
                botonAceptar.SetActive(true);
            }
        }

    }
    public void TamSiguiente()
    {
        pos_tam++;
        bloqueo.SetActive(false);
        botonAceptar.SetActive(true);
        //planetas
        if (pos_elm == 0 || pos_elm == 1 || pos_elm == 2)
        {
            if (pos_tam >= tam.Length)
            {
                pos_tam = 0;
            }
            if (pos_tam < tam.Length && pos_elm >= 0)
            {
                play.transform.localScale = new Vector3(tam[pos_tam], tam[pos_tam], tam[pos_tam]);
                play1.transform.localScale = new Vector3(tam[pos_tam], tam[pos_tam], tam[pos_tam]);
                n_tam.text = nom_tam[pos_tam];
                n_color.text = nom_color_planeta[pos_color];
            }
            //candado planetas
            if (recompensaC.desbloqueo_p[pos_color] == false)
            {
                bloqueo.SetActive(true);
                botonAceptar.SetActive(false);

            }
            else
            {
                bloqueo.SetActive(false);
                botonAceptar.SetActive(true);
            }
        }
        //estrellas y supernovas
        if (pos_elm == 3 || pos_elm == 4)
        {
            if (pos_tam >= tam.Length)
            {
                pos_tam = 0;
            }
            if (pos_tam < tam.Length && pos_elm >= 0)
            {
                play.transform.localScale = new Vector3(posx[pos_tam], posx[pos_tam], posx[pos_tam]);
                play1.transform.localScale = new Vector3(posx[pos_tam], posx[pos_tam], posx[pos_tam]);
                n_tam.text = nom_tam[pos_tam];
                if (pos_elm == 3)
                {
                    n_color.text = nom_color_estrella[pos_color];
                }
                else if (pos_elm == 4)
                {
                    n_color.text = nom_color_supernova_nebulosa[pos_color];
                }
            }
            //candado estrellas
            if (recompensaC.desbloqueo_e[pos_color] == false)
            {
                bloqueo.SetActive(true);
                botonAceptar.SetActive(false);
            }
            else
            {
                bloqueo.SetActive(false);
                botonAceptar.SetActive(true);
            }
            //candado supernova
            if (recompensaC.desbloqueo_s[pos_color] == false)
            {
                bloqueo.SetActive(true);
                botonAceptar.SetActive(false);
            }
            else
            {
                bloqueo.SetActive(false);
                botonAceptar.SetActive(true);
            }
        }
        //nebulosas, galaxias
        if (pos_elm == 5 || pos_elm == 6)
        {
            if (pos_tam >= tam.Length)
            {
                pos_tam = 0;
            }
            if (pos_tam < tam.Length && pos_elm >= 0)
            {
                play.transform.localScale = new Vector3(posgx[pos_tam], posgx[pos_tam], posgx[pos_tam]);
                play1.transform.localScale = new Vector3(posgx[pos_tam], posgx[pos_tam], posgx[pos_tam]);
                n_tam.text = nom_tam[pos_tam];
                if (pos_elm == 5)
                {
                    n_color.text = nom_color_supernova_nebulosa[pos_color];
                }
                else if (pos_elm == 6)
                {
                    n_color.text = nom_color_galaxia[pos_color];
                }
            }
            //candado nebulosa
            if (recompensaC.desbloqueo_n[pos_color] == false)
            {
                bloqueo.SetActive(true);
                botonAceptar.SetActive(false);
            }
            else
            {
                bloqueo.SetActive(false);
                botonAceptar.SetActive(true);
            }
            //candado galaxia
            if (recompensaC.desbloqueo_g[pos_color] == false)
            {
                bloqueo.SetActive(true);
                botonAceptar.SetActive(false);
            }
            else
            {
                bloqueo.SetActive(false);
                botonAceptar.SetActive(true);
            }
        }
    }
    //color-------------------------------------------------------------------------
    public void ColorAnterior()
    {
        bloqueo.SetActive(false);
        botonAceptar.SetActive(true);
        Renderer rend = play.GetComponent<Renderer>();
        Renderer rend1 = play1.GetComponent<Renderer>();
        pos_color--;
        //planetas
        if (pos_elm == 0 || pos_elm == 1 || pos_elm == 2)
        {
            if (pos_color < 0)
            {
                pos_color = material_p.Length - 1;
            }
            if (pos_color < material_p.Length && pos_color >= 0)
            {
                rend.sharedMaterial = material_p[pos_color];
                rend1.sharedMaterial = material_p[pos_color];
                n_color.text = nom_color_planeta[pos_color];
            }
            //candado planetas
            if (recompensaC.desbloqueo_p[pos_color] == false)
            {
                bloqueo.SetActive(true);
                botonAceptar.SetActive(false);

            }
            else
            {
                bloqueo.SetActive(false);
                botonAceptar.SetActive(true);
            }
        }
        //estrella
        if (pos_elm == 3)
        {
            if (pos_color < 0)
            {
                pos_color = material_e.Length - 1;
            }
            if (pos_color < material_e.Length && pos_color >= 0)
            {
                rend.sharedMaterial = material_e[pos_color];
                rend1.sharedMaterial = material_e[pos_color];
                n_color.text = nom_color_estrella[pos_color];
            }
            //candado estrellas
            if (recompensaC.desbloqueo_e[pos_color] == false)
            {
                bloqueo.SetActive(true);
                botonAceptar.SetActive(false);
            }
            else
            {
                bloqueo.SetActive(false);
                botonAceptar.SetActive(true);
            }
        }
        //snova
        else if (pos_elm == 4)
        {
            if (pos_color < 0)
            {
                pos_color = material_s.Length - 1;
            }
            if (pos_color < material_s.Length && pos_color >= 0)
            {
                rend.sharedMaterial.mainTexture = material_s[pos_color];
                rend1.sharedMaterial.mainTexture = material_s[pos_color];
                n_color.text = nom_color_supernova_nebulosa[pos_color];
            }
            //candado supernova
            if (recompensaC.desbloqueo_s[pos_color] == false)
            {
                bloqueo.SetActive(true);
                botonAceptar.SetActive(false);
            }
            else
            {
                bloqueo.SetActive(false);
                botonAceptar.SetActive(true);
            }
        }
        //nebulosa
        else if (pos_elm == 5)
        {
            if (pos_color < 0)
            {
                pos_color = material_n.Length - 1;
            }
            if (pos_color < material_n.Length && pos_color >= 0)
            {
                rend.sharedMaterial.mainTexture = material_n[pos_color];
                rend1.sharedMaterial.mainTexture = material_n[pos_color];
                n_color.text = nom_color_supernova_nebulosa[pos_color];
            }
            //candado nebulosa
            if (recompensaC.desbloqueo_n[pos_color] == false)
            {
                bloqueo.SetActive(true);
                botonAceptar.SetActive(false);
            }
            else
            {
                bloqueo.SetActive(false);
                botonAceptar.SetActive(true);
            }
        }
        //galaxia
        else if (pos_elm == 6)
        {
            if (pos_color < 0)
            {
                pos_color = material_g.Length - 1;
            }
            if (pos_color < material_g.Length && pos_color >= 0)
            {
                rend.sharedMaterial = material_g[pos_color];
                rend1.sharedMaterial = material_g[pos_color];
                n_color.text = nom_color_galaxia[pos_color];
            }
            //candado galaxia
            if (recompensaC.desbloqueo_g[pos_color] == false)
            {
                bloqueo.SetActive(true);
                botonAceptar.SetActive(false);
            }
            else
            {
                bloqueo.SetActive(false);
                botonAceptar.SetActive(true);
            }
        }
    }
    public void ColorSiguiente()
    {
        bloqueo.SetActive(false);
        botonAceptar.SetActive(true);
        Renderer rend = play.GetComponent<Renderer>();
        Renderer rend1 = play1.GetComponent<Renderer>();
        pos_color++;
        //planetas
        if (pos_elm == 0 || pos_elm == 1 || pos_elm == 2)
        {
            if (pos_color >= material_p.Length)
            {
                pos_color = 0;
            }
            if (pos_color < material_p.Length && pos_color >= 0)
            {
                rend.sharedMaterial = material_p[pos_color];
                rend1.sharedMaterial = material_p[pos_color];
                n_color.text = nom_color_planeta[pos_color];
            }
            //candado planetas
            if (recompensaC.desbloqueo_p[pos_color] == false)
            {
                bloqueo.SetActive(true);
                botonAceptar.SetActive(false);
            }
            else
            {
                bloqueo.SetActive(false);
                botonAceptar.SetActive(true);
            }
        }
        //estrella
        else if (pos_elm == 3)
        {
            if (pos_color >= material_e.Length)
            {
                pos_color = 0;
            }
            if (pos_color < material_e.Length && pos_color >= 0)
            {
                rend.sharedMaterial = material_e[pos_color];
                rend1.sharedMaterial = material_e[pos_color];
                n_color.text = nom_color_estrella[pos_color];
            }
            //candado estrellas
            if (recompensaC.desbloqueo_e[pos_color] == false)
            {
                bloqueo.SetActive(true);
                botonAceptar.SetActive(false);
            }
            else
            {
                bloqueo.SetActive(false);
                botonAceptar.SetActive(true);
            }
        }
        //snova
        else if (pos_elm == 4)
        {
            if (pos_color >= material_s.Length)
            {
                pos_color = 0;
            }
            if (pos_color < material_s.Length && pos_color >= 0)
            {
                rend.sharedMaterial.mainTexture = material_s[pos_color];
                rend1.sharedMaterial.mainTexture = material_s[pos_color];
                n_color.text = nom_color_supernova_nebulosa[pos_color];
            }
            //candado supernova
            if (recompensaC.desbloqueo_s[pos_color] == false)
            {
                bloqueo.SetActive(true);
                botonAceptar.SetActive(false);
            }
            else
            {
                bloqueo.SetActive(false);
                botonAceptar.SetActive(true);
            }
        }
        //nebulosa
        else if (pos_elm == 5)
        {
            if (pos_color >= material_n.Length)
            {
                pos_color = 0;
            }
            if (pos_color < material_n.Length && pos_color >= 0)
            {
                rend.sharedMaterial.mainTexture = material_n[pos_color];
                rend1.sharedMaterial.mainTexture = material_n[pos_color];
                n_color.text = nom_color_supernova_nebulosa[pos_color];
            }
            //candado nebulosa
            if (recompensaC.desbloqueo_n[pos_color] == false)
            {
                bloqueo.SetActive(true);
                botonAceptar.SetActive(false);
            }
            else
            {
                bloqueo.SetActive(false);
                botonAceptar.SetActive(true);
            }
        }
        //galaxia
        else if (pos_elm == 6)
        {
            if (pos_color >= material_g.Length)
            {
                pos_color = 0;
            }
            if (pos_color < material_g.Length && pos_color >= 0)
            {
                rend.sharedMaterial = material_g[pos_color];
                rend1.sharedMaterial = material_g[pos_color];
                n_color.text = nom_color_galaxia[pos_color];
            }
            //candado galaxia
            if (recompensaC.desbloqueo_g[pos_color] == false)
            {
                bloqueo.SetActive(true);
                botonAceptar.SetActive(false);
            }
            else
            {
                bloqueo.SetActive(false);
                botonAceptar.SetActive(true);
            }
        }
    }
    //Aceptar---------------------------------------------
    
    public void AceptarObjeto()
    {
        if (pos_elm == 0)
        {
            conexiones.crearPlaneta();
        }
        if (pos_elm == 1)
        {
            conexiones.crearPlanetaS();
        }
        if (pos_elm == 2)
        {
            conexiones.crearPlanetaA();
        }
        if (pos_elm == 3)
        {
            conexiones.crearEstrella();
        }
        if (pos_elm == 4)
        {
            conexiones.crearSuper();
        }
        if (pos_elm == 5)
        {
            conexiones.crearNebu();
        }
        if (pos_elm == 6)
        {
            conexiones.crearGala();
        }
        cont++;
        if (cont <= maximo)
        {
            numCreado = cont;
            //play.transform.parent = ElementoFinal.transform;
            play.SetActive(true);

        }/* 
		else
		{
			//mensaje.SetActive(true);
			botonAceptar.SetActive(false);
		}*/
    }
    //Mensaje de error
    /* public void AceptarMensaje(){
		mensaje.SetActive(false);
		botonAceptar.SetActive(true);
	}*/
    //Boton Volver
    public void Volver()
    {
        Destroy(play);
        Destroy(play1);
        //Destroy(ElementoFinal);
    }
    void Awake(){
        DontDestroyOnLoad(gameObject);
    }
    void Start()
    {
        play = Instantiate(elemento[0]) as GameObject;
        play1 = Instantiate(elemento1[0]) as GameObject;
        //ElementoFinal = Instantiate (ElementoF) as GameObject;
        play.SetActive(true);
        play1.SetActive(true);
        n_elemento.text = nom_elemento[0];
        n_color.text = nom_color_planeta[0];
        n_tam.text = nom_tam[0];
        bloqueo.SetActive(false);
        //mensaje.SetActive(false);
        botonAceptar.SetActive(true);
        //play.SetActive(false);
        //play.transform.parent=ElementoFinal.transform;
        
    }
    void Update()
    {
    }
}