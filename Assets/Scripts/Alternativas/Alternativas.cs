﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class Alternativas : MonoBehaviour
{
    //public Text shakeCountText;
    public int reset = 0;
    [SerializeField] private string[] pregunta;
    [SerializeField] private Text preguntaGO;
    [SerializeField] private string condicion;
    [SerializeField] private Text condicionGO;
    [SerializeField] private string[] justificacionCorrecta;
    [SerializeField] private string[] justificacionIncorrecta1;
    [SerializeField] private string[] justificacionIncorrecta2;
    [SerializeField] private string[] justificacionIncorrecta3;
    [SerializeField] private Text justificacionGO;
    [SerializeField] private GameObject justificacionGOb;
    [SerializeField] private string[] alternativasCorrecta;
    [SerializeField] private string[] alternativasIncorrecta1;
    [SerializeField] private string[] alternativasIncorrecta2;
    [SerializeField] private string[] alternativasIncorrecta3;
    [SerializeField] private Text alternativaA;
    [SerializeField] private Text alternativaB;
    [SerializeField] private Text alternativaC;
    [SerializeField] private Text alternativaD;
    public ShakeDemo shakeDemo;
    public GameObject A;
    public GameObject B;
    public GameObject C;
    public GameObject D;
    public GameObject Acomplet;
    public GameObject Bcomplet;
    public GameObject Ccomplet;
    public GameObject Dcomplet;
    public GameObject Bloqueo;
    [SerializeField] private GameObject Panel;
    [SerializeField] private GameObject TextCorrecto;
    [SerializeField] private GameObject TextIncorrecto;
    [SerializeField] private GameObject podo;
    public Conexiones conexiones;
    public int cantidad_preguntas;
    private int cont_preguntas;
    private int n_pregunta;
    private int n_condicion = 0;
    public bool correcta;
    public bool incorrecta;
    private int AlternativaCorrecta;
    private Renderer rend;
    private Renderer rend1;
    private Renderer rend2;
    private Renderer rend3;
    public void cambiaPregunta()
    {

        Renderer rend = A.GetComponent<Renderer>();
        Renderer rend1 = B.GetComponent<Renderer>();
        Renderer rend2 = C.GetComponent<Renderer>();
        Renderer rend3 = D.GetComponent<Renderer>();
        n_pregunta = Random.Range(0, pregunta.Length);
        n_condicion = 10;

        if (cont_preguntas == cantidad_preguntas - 1)
        {
            Panel.SetActive(true);
        }
        else
        {
            preguntaGO.text = pregunta[n_pregunta];
            condicionGO.text = condicion;
            cont_preguntas++;
        }

        TextCorrecto.SetActive(false);
        TextIncorrecto.SetActive(false);
        justificacionGOb.SetActive(false);
        podo.SetActive(true);
        justificacionGO.text = " ";
        rend.material.color = Color.gray;
        rend1.material.color = Color.gray;
        rend2.material.color = Color.gray;
        rend3.material.color = Color.gray;
        Acomplet.SetActive(false);
        Bcomplet.SetActive(false);
        Ccomplet.SetActive(false);
        Dcomplet.SetActive(false);
        shakeDemo.ResetShakeCount();


    }

    public void UbicaRespuesta()
    {
        AlternativaCorrecta = Random.Range(0, 3);
        //if(shakeDemo.pasos>=(n_condicion)){
        //if(pasos>=(n_condicion*3)){
        if (AlternativaCorrecta == 0)
        {
            alternativaA.text = alternativasCorrecta[n_pregunta];
            alternativaB.text = alternativasIncorrecta1[n_pregunta];
            alternativaC.text = alternativasIncorrecta2[n_pregunta];
            alternativaD.text = alternativasIncorrecta3[n_pregunta];
        }
        else if (AlternativaCorrecta == 1)
        {
            alternativaA.text = alternativasIncorrecta2[n_pregunta];
            alternativaB.text = alternativasCorrecta[n_pregunta];
            alternativaC.text = alternativasIncorrecta3[n_pregunta];
            alternativaD.text = alternativasIncorrecta1[n_pregunta];
        }
        else if (AlternativaCorrecta == 2)
        {
            alternativaA.text = alternativasIncorrecta2[n_pregunta];
            alternativaB.text = alternativasIncorrecta3[n_pregunta];
            alternativaC.text = alternativasCorrecta[n_pregunta];
            alternativaD.text = alternativasIncorrecta1[n_pregunta];
        }
        else
        {
            alternativaA.text = alternativasIncorrecta3[n_pregunta];
            alternativaB.text = alternativasIncorrecta2[n_pregunta];
            alternativaC.text = alternativasIncorrecta1[n_pregunta];
            alternativaD.text = alternativasCorrecta[n_pregunta];
        }
        //}

    }

    public void Seleccion_A()
    {
        rend = A.GetComponent<Renderer>();
        rend1 = B.GetComponent<Renderer>();
        rend2 = C.GetComponent<Renderer>();
        rend3 = D.GetComponent<Renderer>();
        //correcta
        if (alternativaA.text == alternativasCorrecta[n_pregunta])
        {
            justificacionGO.text = justificacionCorrecta[n_pregunta];
            rend.material.color = Color.green;
            correcta = true;
            incorrecta = false;
            TextCorrecto.SetActive(true);
            justificacionGOb.SetActive(true);
            podo.SetActive(false);
            Bloqueo.SetActive(true);
            Invoke("shakedemo.ResetShakeCount", 7);
            Invoke("cambiaPregunta", 7);
            Invoke("UbicaRespuesta", 7);
            Invoke("ocultar", 7);
            conexiones.CorrectaA();

        }
        //incorrecta 1
        else if (alternativaA.text == alternativasIncorrecta1[n_pregunta])
        {
            incorrecta = true;
            correcta = false;
            TextIncorrecto.SetActive(true);
            justificacionGOb.SetActive(true);
            podo.SetActive(false);
            Bloqueo.SetActive(true);
            justificacionGO.text = justificacionIncorrecta1[n_pregunta];
            rend.material.color = Color.red;
            if (alternativaB.text == alternativasCorrecta[n_pregunta])
            {
                rend1.material.color = Color.blue;
            }
            else if (alternativaC.text == alternativasCorrecta[n_pregunta])
            {
                rend2.material.color = Color.blue;
            }
            else if (alternativaD.text == alternativasCorrecta[n_pregunta])
            {
                rend3.material.color = Color.blue;
            }
            Invoke("shakedemo.ResetShakeCount", 7);
            Invoke("cambiaPregunta", 7);
            Invoke("UbicaRespuesta", 7);
            Invoke("ocultar", 7);
            conexiones.IncorrectaA();
        }
        //incorrecta 2
        else if (alternativaA.text == alternativasIncorrecta2[n_pregunta])
        {
            incorrecta = true;
            correcta = false;
            TextIncorrecto.SetActive(true);
            justificacionGOb.SetActive(true);
            podo.SetActive(false);
            Bloqueo.SetActive(true);
            justificacionGO.text = justificacionIncorrecta2[n_pregunta];
            rend.material.color = Color.red;
            if (alternativaB.text == alternativasCorrecta[n_pregunta])
            {
                rend1.material.color = Color.blue;
            }
            else if (alternativaC.text == alternativasCorrecta[n_pregunta])
            {
                rend2.material.color = Color.blue;
            }
            else if (alternativaD.text == alternativasCorrecta[n_pregunta])
            {
                rend3.material.color = Color.blue;
            }
            Invoke("shakedemo.ResetShakeCount", 7);
            Invoke("cambiaPregunta", 7);
            Invoke("UbicaRespuesta", 7);
            Invoke("ocultar", 7);
            conexiones.IncorrectaA();
        }
        //incorrecta 3
        else if (alternativaA.text == alternativasIncorrecta3[n_pregunta])
        {
            incorrecta = true;
            correcta = false;
            TextIncorrecto.SetActive(true);
            justificacionGOb.SetActive(true);
            podo.SetActive(false);
            Bloqueo.SetActive(true);
            justificacionGO.text = justificacionIncorrecta3[n_pregunta];
            rend.material.color = Color.red;
            if (alternativaB.text == alternativasCorrecta[n_pregunta])
            {
                rend1.material.color = Color.blue;
            }
            else if (alternativaC.text == alternativasCorrecta[n_pregunta])
            {
                rend2.material.color = Color.blue;
            }
            else if (alternativaD.text == alternativasCorrecta[n_pregunta])
            {
                rend3.material.color = Color.blue;
            }
            Invoke("shakedemo.ResetShakeCount", 7);
            Invoke("cambiaPregunta", 7);
            Invoke("UbicaRespuesta", 7);
            Invoke("ocultar", 7);
            conexiones.IncorrectaA();
        }
    }
    public void Seleccion_B()
    {
        Renderer rend = B.GetComponent<Renderer>();
        Renderer rend1 = A.GetComponent<Renderer>();
        Renderer rend2 = C.GetComponent<Renderer>();
        Renderer rend3 = D.GetComponent<Renderer>();
        //correcta
        if (alternativaB.text == alternativasCorrecta[n_pregunta])
        {
            justificacionGO.text = justificacionCorrecta[n_pregunta];
            rend.material.color = Color.green;
            correcta = true;
            incorrecta = false;
            TextCorrecto.SetActive(true);
            justificacionGOb.SetActive(true);
            Bloqueo.SetActive(true);
            Invoke("shakedemo.ResetShakeCount", 7);
            Invoke("cambiaPregunta", 7);
            Invoke("UbicaRespuesta", 7);
            Invoke("ocultar", 7);
            conexiones.CorrectaB();
        }
        //incorrecta 1
        else if (alternativaB.text == alternativasIncorrecta1[n_pregunta])
        {
            incorrecta = true;
            correcta = false;
            TextIncorrecto.SetActive(true);
            justificacionGOb.SetActive(true);
            podo.SetActive(false);
            Bloqueo.SetActive(true);
            justificacionGO.text = justificacionIncorrecta1[n_pregunta];
            rend.material.color = Color.red;
            if (alternativaA.text == alternativasCorrecta[n_pregunta])
            {
                rend1.material.color = Color.blue;
            }
            else if (alternativaC.text == alternativasCorrecta[n_pregunta])
            {
                rend2.material.color = Color.blue;
            }
            else if (alternativaD.text == alternativasCorrecta[n_pregunta])
            {
                rend3.material.color = Color.blue;
            }
            Invoke("shakedemo.ResetShakeCount", 7);
            Invoke("cambiaPregunta", 7);
            Invoke("UbicaRespuesta", 7);
            Invoke("ocultar", 7);
            conexiones.IncorrectaB();
        }
        //incorrecta 2
        else if (alternativaB.text == alternativasIncorrecta2[n_pregunta])
        {
            incorrecta = true;
            correcta = false;
            TextIncorrecto.SetActive(true);
            justificacionGOb.SetActive(true);
            podo.SetActive(false);
            Bloqueo.SetActive(true);
            justificacionGO.text = justificacionIncorrecta2[n_pregunta];
            rend.material.color = Color.red;
            if (alternativaA.text == alternativasCorrecta[n_pregunta])
            {
                rend1.material.color = Color.blue;
            }
            else if (alternativaC.text == alternativasCorrecta[n_pregunta])
            {
                rend2.material.color = Color.blue;
            }
            else if (alternativaD.text == alternativasCorrecta[n_pregunta])
            {
                rend3.material.color = Color.blue;
            }
            Invoke("shakedemo.ResetShakeCount", 7);
            Invoke("cambiaPregunta", 7);
            Invoke("UbicaRespuesta", 7);
            Invoke("ocultar", 7);
            conexiones.IncorrectaB();
        }
        //incorrecta 3
        else if (alternativaB.text == alternativasIncorrecta3[n_pregunta])
        {
            incorrecta = true;
            correcta = false;
            TextIncorrecto.SetActive(true);
            justificacionGOb.SetActive(true);
            podo.SetActive(false);
            Bloqueo.SetActive(true);
            justificacionGO.text = justificacionIncorrecta3[n_pregunta];
            rend.material.color = Color.red;
            if (alternativaA.text == alternativasCorrecta[n_pregunta])
            {
                rend1.material.color = Color.blue;
            }
            else if (alternativaC.text == alternativasCorrecta[n_pregunta])
            {
                rend2.material.color = Color.blue;
            }
            else if (alternativaD.text == alternativasCorrecta[n_pregunta])
            {
                rend3.material.color = Color.blue;
            }
            Invoke("shakedemo.ResetShakeCount", 7);
            Invoke("cambiaPregunta", 7);
            Invoke("UbicaRespuesta", 7);
            Invoke("ocultar", 7);
            conexiones.IncorrectaB();
        }
    }
    public void Seleccion_C()
    {
        Renderer rend = C.GetComponent<Renderer>();
        Renderer rend1 = B.GetComponent<Renderer>();
        Renderer rend2 = A.GetComponent<Renderer>();
        Renderer rend3 = D.GetComponent<Renderer>();
        //correcta
        if (alternativaC.text == alternativasCorrecta[n_pregunta])
        {
            justificacionGO.text = justificacionCorrecta[n_pregunta];
            rend.material.color = Color.green;
            correcta = true;
            incorrecta = false;
            TextCorrecto.SetActive(true);
            justificacionGOb.SetActive(true);
            podo.SetActive(false);
            Bloqueo.SetActive(true);
            Invoke("shakedemo.ResetShakeCount", 7);
            Invoke("cambiaPregunta", 7);
            Invoke("UbicaRespuesta", 7);
            Invoke("ocultar", 7);
            conexiones.CorrectaC();
        }
        //incorrecta 1
        else if (alternativaC.text == alternativasIncorrecta1[n_pregunta])
        {
            incorrecta = true;
            correcta = false;
            TextIncorrecto.SetActive(true);
            justificacionGOb.SetActive(true);
            podo.SetActive(false);
            Bloqueo.SetActive(true);
            justificacionGO.text = justificacionIncorrecta1[n_pregunta];
            rend.material.color = Color.red;
            if (alternativaB.text == alternativasCorrecta[n_pregunta])
            {
                rend1.material.color = Color.blue;
            }
            else if (alternativaA.text == alternativasCorrecta[n_pregunta])
            {
                rend2.material.color = Color.blue;
            }
            else if (alternativaD.text == alternativasCorrecta[n_pregunta])
            {
                rend3.material.color = Color.blue;
            }
            Invoke("shakedemo.ResetShakeCount", 7);
            Invoke("cambiaPregunta", 7);
            Invoke("UbicaRespuesta", 7);
            Invoke("ocultar", 7);
            conexiones.IncorrectaC();
        }
        //incorrecta 2
        else if (alternativaC.text == alternativasIncorrecta2[n_pregunta])
        {
            incorrecta = true;
            correcta = false;
            TextIncorrecto.SetActive(true);
            justificacionGOb.SetActive(true);
            podo.SetActive(false);
            Bloqueo.SetActive(true);
            justificacionGO.text = justificacionIncorrecta2[n_pregunta];
            rend.material.color = Color.red;
            if (alternativaB.text == alternativasCorrecta[n_pregunta])
            {
                rend1.material.color = Color.blue;
            }
            else if (alternativaA.text == alternativasCorrecta[n_pregunta])
            {
                rend2.material.color = Color.blue;
            }
            else if (alternativaD.text == alternativasCorrecta[n_pregunta])
            {
                rend3.material.color = Color.blue;
            }
            Invoke("shakedemo.ResetShakeCount", 7);
            Invoke("cambiaPregunta", 7);
            Invoke("UbicaRespuesta", 7);
            Invoke("ocultar", 7);
            conexiones.IncorrectaC();
        }
        //incorrecta 3
        else if (alternativaC.text == alternativasIncorrecta3[n_pregunta])
        {
            incorrecta = true;
            correcta = false;
            TextIncorrecto.SetActive(true);
            justificacionGOb.SetActive(true);
            podo.SetActive(false);
            Bloqueo.SetActive(true);
            justificacionGO.text = justificacionIncorrecta3[n_pregunta];
            rend.material.color = Color.red;
            if (alternativaB.text == alternativasCorrecta[n_pregunta])
            {
                rend1.material.color = Color.blue;
            }
            else if (alternativaA.text == alternativasCorrecta[n_pregunta])
            {
                rend2.material.color = Color.blue;
            }
            else if (alternativaD.text == alternativasCorrecta[n_pregunta])
            {
                rend3.material.color = Color.blue;
            }
            Invoke("shakedemo.ResetShakeCount", 7);
            Invoke("cambiaPregunta", 7);
            Invoke("UbicaRespuesta", 7);
            Invoke("ocultar", 7);
            conexiones.IncorrectaC(); 
        }
    }
    public void Seleccion_D()
    {
        Renderer rend = D.GetComponent<Renderer>();
        Renderer rend1 = B.GetComponent<Renderer>();
        Renderer rend2 = C.GetComponent<Renderer>();
        Renderer rend3 = A.GetComponent<Renderer>();
        //correcta
        if (alternativaD.text == alternativasCorrecta[n_pregunta])
        {
            justificacionGO.text = justificacionCorrecta[n_pregunta];
            rend.material.color = Color.green;
            correcta = true;
            incorrecta = false;
            TextCorrecto.SetActive(true);
            justificacionGOb.SetActive(true);
            podo.SetActive(false);
            Bloqueo.SetActive(true);
            Invoke("shakedemo.ResetShakeCount", 7);
            Invoke("cambiaPregunta", 7);
            Invoke("UbicaRespuesta", 7);
            Invoke("ocultar", 7);
            conexiones.CorrectaD();
        }
        //incorrecta 1
        else if (alternativaD.text == alternativasIncorrecta1[n_pregunta])
        {
            incorrecta = true;
            correcta = false;
            TextIncorrecto.SetActive(true);
            justificacionGOb.SetActive(true);
            podo.SetActive(false);
            Bloqueo.SetActive(true);
            justificacionGO.text = justificacionIncorrecta1[n_pregunta];
            rend.material.color = Color.red;
            if (alternativaB.text == alternativasCorrecta[n_pregunta])
            {
                rend1.material.color = Color.blue;
            }
            else if (alternativaC.text == alternativasCorrecta[n_pregunta])
            {
                rend2.material.color = Color.blue;
            }
            else if (alternativaA.text == alternativasCorrecta[n_pregunta])
            {
                rend3.material.color = Color.blue;
            }
            Invoke("shakedemo.ResetShakeCount", 7);
            Invoke("cambiaPregunta", 7);
            Invoke("UbicaRespuesta", 7);
            Invoke("ocultar", 7);
            conexiones.IncorrectaD();
        }
        //incorrecta 2
        else if (alternativaD.text == alternativasIncorrecta2[n_pregunta])
        {
            incorrecta = true;
            correcta = false;
            TextIncorrecto.SetActive(true);
            justificacionGOb.SetActive(true);
            podo.SetActive(false);
            Bloqueo.SetActive(true);
            justificacionGO.text = justificacionIncorrecta2[n_pregunta];
            rend.material.color = Color.red;
            if (alternativaB.text == alternativasCorrecta[n_pregunta])
            {
                rend1.material.color = Color.blue;
            }
            else if (alternativaC.text == alternativasCorrecta[n_pregunta])
            {
                rend2.material.color = Color.blue;
            }
            else if (alternativaA.text == alternativasCorrecta[n_pregunta])
            {
                rend3.material.color = Color.blue;
            }
            Invoke("shakedemo.ResetShakeCount", 7);
            Invoke("cambiaPregunta", 7);
            Invoke("UbicaRespuesta", 7);
            Invoke("ocultar", 7);
            conexiones.IncorrectaD();
        }
        //incorrecta 3
        else if (alternativaD.text == alternativasIncorrecta3[n_pregunta])
        {
            incorrecta = true;
            correcta = false;
            TextIncorrecto.SetActive(true);
            justificacionGOb.SetActive(true);
            podo.SetActive(false);
            Bloqueo.SetActive(true);
            justificacionGO.text = justificacionIncorrecta3[n_pregunta];
            rend.material.color = Color.red;
            if (alternativaB.text == alternativasCorrecta[n_pregunta])
            {
                rend1.material.color = Color.blue;
            }
            else if (alternativaC.text == alternativasCorrecta[n_pregunta])
            {
                rend2.material.color = Color.blue;
            }
            else if (alternativaA.text == alternativasCorrecta[n_pregunta])
            {
                rend3.material.color = Color.blue;
            }
            Invoke("shakedemo.ResetShakeCount", 7);
            Invoke("cambiaPregunta", 7);
            Invoke("UbicaRespuesta", 7);
            Invoke("UbicaRespuesta", 7);
            Invoke("ocultar", 7);
            conexiones.IncorrectaD();
        }
    }

    public void aceptarRecompensa()
    {

    }
    // Start is called before the first frame update
    void Start()
    {
        Renderer rend = A.GetComponent<Renderer>();
        Renderer rend1 = B.GetComponent<Renderer>();
        Renderer rend2 = C.GetComponent<Renderer>();
        Renderer rend3 = D.GetComponent<Renderer>();
        rend.material.color = Color.gray;
        rend1.material.color = Color.gray;
        rend2.material.color = Color.gray;
        rend3.material.color = Color.gray;
        Panel.SetActive(false);
        TextCorrecto.SetActive(false);
        TextIncorrecto.SetActive(false);
        justificacionGOb.SetActive(false);
        podo.SetActive(true);
        justificacionGO.text = " ";

        cambiaPregunta();
        UbicaRespuesta();
        justificacionGO.text = " ";
        cont_preguntas = 0;

    }

    // Update is called once per frame
    void Update()
    {
        if (shakeDemo.pasos >= n_condicion)
        {
            Acomplet.SetActive(true);
            Bcomplet.SetActive(true);
            Ccomplet.SetActive(true);
            Dcomplet.SetActive(true);
        }
        else if (shakeDemo.pasos == 0)
        {
            Acomplet.SetActive(false);
            Bcomplet.SetActive(false);
            Ccomplet.SetActive(false);
            Dcomplet.SetActive(false);
        }


    }

    public void ocultar()
    {
        Bloqueo.SetActive(false);
    }
}