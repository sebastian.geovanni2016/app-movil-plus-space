﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlipCard : MonoBehaviour
{
    public int fps=60;
    public int rotateDegreePerSecond=180;
    public bool isFaceUp=false;
    float WaitTime;
    bool isAnimationProcessing=false;
    //private bool done=false;
    const float FLIP_LIMIT_DEGREE=180f;
    void Start()
    {
        WaitTime = 1.0f/fps;
    }
    void Update()
    {
        
    }
    void OnMouseDown(){
        if(isAnimationProcessing){
            return;
        }
        StartCoroutine(flip());
        
    }
    IEnumerator flip (){
        isAnimationProcessing=true;
        bool done=false;
        while (!done)
        {
            float degree=rotateDegreePerSecond*Time.deltaTime;
            if (isFaceUp)
            {
                degree=-degree;
            }
            transform.Rotate(new Vector3(0,degree,0));
            if (FLIP_LIMIT_DEGREE<transform.eulerAngles.y){
                transform.Rotate(new Vector3(0,-degree,0));
                done=true;
            }
            
            yield return new WaitForSeconds(WaitTime);
        }
    }
}
