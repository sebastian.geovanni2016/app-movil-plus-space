﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class Logica : MonoBehaviour
{
    public string[] preguntas;
    public Text preguntasGO;
    private int cont_preguntas = 0;
    public int cantidad_preguntas = 5;
    private int n_pregunta = 0;
    public GameObject Panel;
    public int CartaCorrecta_1;
    public int CartaCorrecta_2;
    public int CartaCorrecta_3;
    public int CartaCorrecta_4;
    public float speed;
    public Transform target_J1_1;
    public Transform target_J1_2;
    public Transform target_J1_3;
    public Transform target_J1_4;
    public Transform target_J2_1;
    public Transform target_J2_2;
    public Transform target_J2_3;
    public Transform target_J2_4;
    public Transform target_J3_1;
    public Transform target_J3_2;
    public Transform target_J3_3;
    public Transform target_J3_4;
    public Transform target_J4_1;
    public Transform target_J4_2;
    public Transform target_J4_3;
    public Transform target_J4_4;
    private GameObject play1_1;
    private GameObject play1_2;
    private GameObject play1_3;
    private GameObject play1_4;
    private GameObject play2_1;
    private GameObject play2_2;
    private GameObject play2_3;
    private GameObject play2_4;
    private GameObject play3_1;
    private GameObject play3_2;
    private GameObject play3_3;
    private GameObject play3_4;
    private GameObject play4_1;
    private GameObject play4_2;
    private GameObject play4_3;
    private GameObject play4_4;
    public GameObject[] Correctas1;
    public GameObject[] Correctas2;
    public GameObject[] Correctas3;
    public GameObject[] Correctas4;
    //incorrectas
    public GameObject[] Incorrectas1_1;
    public GameObject[] Incorrectas1_2;
    public GameObject[] Incorrectas1_3;
    //public GameObject[] Incorrectas1_4;
    public GameObject[] Incorrectas2_1;
    public GameObject[] Incorrectas2_2;
    public GameObject[] Incorrectas2_3;
    //public GameObject[] Incorrectas2_4;
    public GameObject[] Incorrectas3_1;
    public GameObject[] Incorrectas3_2;
    public GameObject[] Incorrectas3_3;
    //public GameObject[] Incorrectas3_4;
    public GameObject[] Incorrectas4_1;
    public GameObject[] Incorrectas4_2;
    public GameObject[] Incorrectas4_3;
    //public GameObject[] Incorrectas4_4;
    [SerializeField]private GameObject TextCorrecto1;
    [SerializeField]private GameObject TextIncorrecto1;
    [SerializeField]private GameObject TextCorrecto2;
    [SerializeField]private GameObject TextIncorrecto2;
    [SerializeField]private GameObject TextCorrecto3;
    [SerializeField]private GameObject TextIncorrecto3;
    [SerializeField]private GameObject TextCorrecto4;
    [SerializeField]private GameObject TextIncorrecto4;
    

//respuesta jugador 1
    private bool respuesta1_1;
    private bool respuesta1_2;
    private bool respuesta1_3;
    private bool respuesta1_4;
//respueta jugador 2
    private bool respuesta2_1;
    private bool respuesta2_2;
    private bool respuesta2_3;
    private bool respuesta2_4;
//respueta jugador 3    
    private bool respuesta3_1;
    private bool respuesta3_2;
    private bool respuesta3_3;
    private bool respuesta3_4;
//respuesta jugador 4
    private bool respuesta4_1;
    private bool respuesta4_2;
    private bool respuesta4_3;
    private bool respuesta4_4;

    public GameObject Baraja;
    public GameObject cosas;
    //lobby
    public GameObject Lobby;
    public GameObject AceptarJugadores;

    public int jugadores=1;
    public Text jugador;
    public bool correcta;

    //cambia pregunta
    public void ocultar(){
     Lobby.SetActive(false);
    }
    public void siguiente(){
        jugadores++;
        if (jugadores>=5)
        {
            jugadores=1;
            
        }
        jugador.text=jugadores.ToString();
        //return jugadores;
    }
    public void anterior(){
            jugadores--;
        if (jugadores<=0)
        {
            jugadores=4;
            
        }
        jugador.text=jugadores.ToString();
        //return jugadores;
    }
    public void JugadoresFinales(){
        if (jugadores == 1)
        {
        play2_1.SetActive(false);
        play2_2.SetActive(false);
        play2_3.SetActive(false);
        play2_4.SetActive(false);
        play3_1.SetActive(false);
        play3_2.SetActive(false);
        play3_3.SetActive(false);
        play3_4.SetActive(false);
        play4_1.SetActive(false);
        play4_2.SetActive(false);
        play4_3.SetActive(false);
        play4_4.SetActive(false); 
        }
        else if (jugadores == 2)
        {
        play3_1.SetActive(false);
        play3_2.SetActive(false);
        play3_3.SetActive(false);
        play3_4.SetActive(false);
        play4_1.SetActive(false);
        play4_2.SetActive(false);
        play4_3.SetActive(false);
        play4_4.SetActive(false);  
        }
        else if (jugadores == 3)
        {
        play4_1.SetActive(false);
        play4_2.SetActive(false);
        play4_3.SetActive(false);
        play4_4.SetActive(false);    
        }

    }
        public void cambiaPregunta(){
        if (cont_preguntas==cantidad_preguntas-1)
        {
        Panel.SetActive(true);
        Baraja.SetActive(false);
        TextCorrecto1.SetActive(false);
        TextIncorrecto1.SetActive(false);
        TextCorrecto2.SetActive(false);
        TextIncorrecto2.SetActive(false);
        TextCorrecto3.SetActive(false);
        TextIncorrecto3.SetActive(false);
        TextCorrecto4.SetActive(false);
        TextIncorrecto4.SetActive(false);
        play1_1.SetActive(false);
        play1_2.SetActive(false);
        play1_3.SetActive(false);
        play1_4.SetActive(false);
        play2_1.SetActive(false);
        play2_2.SetActive(false);
        play2_3.SetActive(false);
        play2_4.SetActive(false);
        play3_1.SetActive(false);
        play3_2.SetActive(false);
        play3_3.SetActive(false);
        play3_4.SetActive(false);
        play4_1.SetActive(false);
        play4_2.SetActive(false);
        play4_3.SetActive(false);
        play4_4.SetActive(false);
        cosas.SetActive(false);
        preguntasGO.text=" ";
        
        }
        else{
        n_pregunta = Random.Range(0,preguntas.Length);
        preguntasGO.text = preguntas[n_pregunta];
        cont_preguntas++;
        TextCorrecto1.SetActive(false);
        TextIncorrecto1.SetActive(false);
        TextCorrecto2.SetActive(false);
        TextIncorrecto2.SetActive(false);
        TextCorrecto3.SetActive(false);
        TextIncorrecto3.SetActive(false);
        TextCorrecto4.SetActive(false);
        TextIncorrecto4.SetActive(false);
         Destruye(n_pregunta);
         Reparte_J1();
        
        }
        
        }

    public void Destruye(int n_pregunta){
        //destruye cartas 
        Destroy (play1_1);
        Destroy (play1_2);
        Destroy (play1_3);
        Destroy (play1_4);

        Destroy (play2_1);
        Destroy (play2_2);
        Destroy (play2_3);
        Destroy (play2_4);

        Destroy (play3_1);
        Destroy (play3_2);
        Destroy (play3_3);
        Destroy (play3_4);

        Destroy (play4_1);
        Destroy (play4_2);
        Destroy (play4_3);
        Destroy (play4_4);

        //Posicion Correcta
        CartaCorrecta_1=Random.Range(0,4);
        CartaCorrecta_2=Random.Range(0,4);
        CartaCorrecta_3=Random.Range(0,4);
        CartaCorrecta_4=Random.Range(0,4);
    ////////////////////////////////////////////////////
       //J1
        if (CartaCorrecta_1==0)
        {
            //asigna la correcta
            //play1_1 = Instantiate (Correctas1[n_pregunta]) as GameObject;
            play1_1 = Instantiate (Correctas1[n_pregunta]) as GameObject;
            respuesta1_1=true;
            respuesta1_2=false;
            respuesta1_3=false;
            respuesta1_4=false;
            //cambia si las siguientes alternativas son iguales
            
            play1_2 = Instantiate (Incorrectas1_1[n_pregunta]) as GameObject;
            play1_3 = Instantiate (Incorrectas1_2[n_pregunta]) as GameObject;
            play1_4 = Instantiate (Incorrectas1_3[n_pregunta]) as GameObject;
        }
        else if (CartaCorrecta_1==1)
        {
            
            //asigna la correcta
            //play1_2 = Instantiate (Correctas1[n_pregunta]) as GameObject;
            play1_2 = Instantiate (Correctas1[n_pregunta]) as GameObject;
            respuesta1_1=false;
            respuesta1_2=true;
            respuesta1_3=false;
            respuesta1_4=false;
            //cambia si las siguientes alternativas son iguales
            
            play1_4 = Instantiate (Incorrectas1_1[n_pregunta]) as GameObject;
            play1_3 = Instantiate (Incorrectas1_2[n_pregunta]) as GameObject;
            play1_1 = Instantiate (Incorrectas1_3[n_pregunta]) as GameObject;
        }
        else if (CartaCorrecta_1==2)
        {
            //asigna la correcta
            //play1_3 = Instantiate (Correctas1[n_pregunta]) as GameObject;
            play1_3 = Instantiate (Correctas1[n_pregunta]) as GameObject;
            respuesta1_1=false;
            respuesta1_2=false;
            respuesta1_3=true;
            respuesta1_4=false;
            //cambia si las siguientes alternativas son iguales
            
            play1_2 = Instantiate (Incorrectas1_1[n_pregunta]) as GameObject;
            play1_4 = Instantiate (Incorrectas1_2[n_pregunta]) as GameObject;
            play1_1 = Instantiate (Incorrectas1_3[n_pregunta]) as GameObject;
        }
        else if (CartaCorrecta_1==3)
        {
           //asigna la correcta
            //play1_4 = Instantiate (Correctas1[n_pregunta]) as GameObject;
            play1_4 = Instantiate (Correctas1[n_pregunta]) as GameObject;
            respuesta1_1=false;
            respuesta1_2=false;
            respuesta1_3=false;
            respuesta1_4=true;
            //cambia si las siguientes alternativas son iguales
            
            play1_2 = Instantiate (Incorrectas1_1[n_pregunta]) as GameObject;
            play1_3 = Instantiate (Incorrectas1_2[n_pregunta]) as GameObject;
            play1_1 = Instantiate (Incorrectas1_3[n_pregunta]) as GameObject; 
        }
        //J2
        if (CartaCorrecta_2==0)
        {
            //asigna la correcta
            play2_1 = Instantiate (Correctas2[n_pregunta]) as GameObject;
            respuesta2_1=true;
            respuesta2_2=false;
            respuesta2_3=false;
            respuesta2_4=false;
            //cambia si las siguientes alternativas son iguales
            
            play2_2 = Instantiate (Incorrectas2_1[n_pregunta]) as GameObject;
            play2_3 = Instantiate (Incorrectas2_2[n_pregunta]) as GameObject;
            play2_4 = Instantiate (Incorrectas2_3[n_pregunta]) as GameObject;
        }
        else if (CartaCorrecta_2==1)
        {
            //asigna la correcta
            play2_2 = Instantiate (Correctas2[n_pregunta]) as GameObject;
            respuesta2_1=false;
            respuesta2_2=true;
            respuesta2_3=false;
            respuesta2_4=false;
            //cambia si las siguientes alternativas son iguales
            
            play2_1 = Instantiate (Incorrectas2_1[n_pregunta]) as GameObject;
            play2_3 = Instantiate (Incorrectas2_2[n_pregunta]) as GameObject;
            play2_4 = Instantiate (Incorrectas2_3[n_pregunta]) as GameObject;
        }
        else if (CartaCorrecta_2==2)
        {
            //asigna la correcta
            play2_3 = Instantiate (Correctas2[n_pregunta]) as GameObject;
            respuesta2_1=false;
            respuesta2_2=false;
            respuesta2_3=true;
            respuesta2_4=false;
            //cambia si las siguientes alternativas son iguales
            
            play2_2 = Instantiate (Incorrectas2_1[n_pregunta]) as GameObject;
            play2_1 = Instantiate (Incorrectas2_2[n_pregunta]) as GameObject;
            play2_4 = Instantiate (Incorrectas2_3[n_pregunta]) as GameObject;
        }
        else if (CartaCorrecta_2==3)
        {
            //asigna la correcta
            play2_4 = Instantiate (Correctas2[n_pregunta]) as GameObject;
            respuesta2_1=false;
            respuesta2_2=false;
            respuesta2_3=false;
            respuesta2_4=true;
            //cambia si las siguientes alternativas son iguales
            
            play2_2 = Instantiate (Incorrectas2_1[n_pregunta]) as GameObject;
            play2_3 = Instantiate (Incorrectas2_2[n_pregunta]) as GameObject;
            play2_1 = Instantiate (Incorrectas2_3[n_pregunta]) as GameObject;
        }
        //J3
        if (CartaCorrecta_3==0)
        {
            //asigna la correcta
            play3_1 = Instantiate (Correctas3[n_pregunta]) as GameObject;
            respuesta3_1=true;
            respuesta3_2=false;
            respuesta3_3=false;
            respuesta3_4=false;
            //cambia si las siguientes alternativas son iguales
            
            play3_2 = Instantiate ((Incorrectas3_1[n_pregunta])) as GameObject;
            play3_3 = Instantiate ((Incorrectas3_2[n_pregunta])) as GameObject;
            play3_4 = Instantiate ((Incorrectas3_3[n_pregunta])) as GameObject;
        }
        else if (CartaCorrecta_3==1)
        {
            //asigna la correcta
            play3_2 = Instantiate (Correctas3[n_pregunta]) as GameObject;
            respuesta3_1=false;
            respuesta3_2=true;
            respuesta3_3=false;
            respuesta3_4=false;
            //cambia si las siguientes alternativas son iguales
            
            play3_1 = Instantiate ((Incorrectas3_1[n_pregunta])) as GameObject;
            play3_3 = Instantiate ((Incorrectas3_2[n_pregunta])) as GameObject;
            play3_4 = Instantiate ((Incorrectas3_3[n_pregunta])) as GameObject;
        }
        else if (CartaCorrecta_3==2)
        {
            //asigna la correcta
            play3_3 = Instantiate (Correctas3[n_pregunta]) as GameObject;
            respuesta3_1=false;
            respuesta3_2=false;
            respuesta3_3=true;
            respuesta3_4=false;
            
            play3_2 = Instantiate ((Incorrectas3_1[n_pregunta])) as GameObject;
            play3_1 = Instantiate ((Incorrectas3_2[n_pregunta])) as GameObject;
            play3_4 = Instantiate ((Incorrectas3_3[n_pregunta])) as GameObject;
        }
        else if (CartaCorrecta_3==3)
        {
            //asigna la correcta
            play3_4 = Instantiate (Correctas3[n_pregunta]) as GameObject;
            respuesta3_1=false;
            respuesta3_2=false;
            respuesta3_3=false;
            respuesta3_4=true;
            
            play3_2 = Instantiate ((Incorrectas3_1[n_pregunta])) as GameObject;
            play3_3 = Instantiate ((Incorrectas3_2[n_pregunta])) as GameObject;
            play3_1 = Instantiate ((Incorrectas3_2[n_pregunta])) as GameObject;
        }
        //J4
        if (CartaCorrecta_4==0)
        {
            //asigna la correcta
            play4_1 = Instantiate (Correctas4[n_pregunta]) as GameObject;
            respuesta4_1=true;
            respuesta4_2=false;
            respuesta4_3=false;
            respuesta4_4=false;
            
            play4_2 = Instantiate ((Incorrectas4_1[n_pregunta])) as GameObject;
            play4_3 = Instantiate ((Incorrectas4_2[n_pregunta])) as GameObject;
            play4_4 = Instantiate ((Incorrectas4_3[n_pregunta])) as GameObject;
        }
        else if (CartaCorrecta_4==1)
        {
            //asigna la correcta
            play4_2 = Instantiate (Correctas4[n_pregunta]) as GameObject;
            respuesta4_1=false;
            respuesta4_2=true;
            respuesta4_3=false;
            respuesta4_4=false;
            
            play4_1 = Instantiate ((Incorrectas4_1[n_pregunta])) as GameObject;
            play4_3 = Instantiate ((Incorrectas4_2[n_pregunta])) as GameObject;
            play4_4 = Instantiate ((Incorrectas4_3[n_pregunta])) as GameObject;
        }
        else if (CartaCorrecta_4==2)
        {
            //asigna la correcta
            play4_3 = Instantiate (Correctas4[n_pregunta]) as GameObject;
            respuesta4_1=false;
            respuesta4_2=false;
            respuesta4_3=true;
            respuesta4_4=false;
            
            play4_2 = Instantiate ((Incorrectas4_1[n_pregunta])) as GameObject;
            play4_1 = Instantiate ((Incorrectas4_2[n_pregunta])) as GameObject;
            play4_4 = Instantiate ((Incorrectas4_3[n_pregunta])) as GameObject;
        }
        else if (CartaCorrecta_4==3)
        {
            //asigna la correcta
            play4_4 = Instantiate (Correctas4[n_pregunta]) as GameObject;
            respuesta4_1=false;
            respuesta4_2=false;
            respuesta4_3=false;
            respuesta4_4=true;
            
            play4_2 = Instantiate ((Incorrectas4_1[n_pregunta])) as GameObject;
            play4_3 = Instantiate ((Incorrectas4_2[n_pregunta])) as GameObject;
            play4_1 = Instantiate ((Incorrectas4_3[n_pregunta])) as GameObject;
        }
        if (jugadores==1)
        {

        play2_1.SetActive(false);
        play2_2.SetActive(false);
        play2_3.SetActive(false);
        play2_4.SetActive(false);
        play3_1.SetActive(false);
        play3_2.SetActive(false);
        play3_3.SetActive(false);
        play3_4.SetActive(false);
        play4_1.SetActive(false);
        play4_2.SetActive(false);
        play4_3.SetActive(false);
        play4_4.SetActive(false);
        }
        if (jugadores==2)
        {

        play3_1.SetActive(false);
        play3_2.SetActive(false);
        play3_3.SetActive(false);
        play3_4.SetActive(false);
        play4_1.SetActive(false);
        play4_2.SetActive(false);
        play4_3.SetActive(false);
        play4_4.SetActive(false);
        }
        if (jugadores==3)
        {
        
        play4_1.SetActive(false);
        play4_2.SetActive(false);
        play4_3.SetActive(false);
        play4_4.SetActive(false);
        }
    }

    //Reparte
    public void Reparte_J1(){
        speed=20;
        Reparte_J2();
        Reparte_J3();
        Reparte_J4();
    }
    public void Reparte_J2(){
        //speed=20;
        play2_1.transform.Rotate(new Vector3(0,0,180));
        play2_2.transform.Rotate(new Vector3(0,0,180));
        play2_3.transform.Rotate(new Vector3(0,0,180));
        play2_4.transform.Rotate(new Vector3(0,0,180));
    }
    public void Reparte_J3(){
        //speed=20;
        play3_1.transform.Rotate(new Vector3(0,0,270));
        play3_2.transform.Rotate(new Vector3(0,0,270));
        play3_3.transform.Rotate(new Vector3(0,0,270));
        play3_4.transform.Rotate(new Vector3(0,0,270));
    }
    public void Reparte_J4(){
        //speed=10;
        play4_1.transform.Rotate(new Vector3(0,0,90));
        play4_2.transform.Rotate(new Vector3(0,0,90));
        play4_3.transform.Rotate(new Vector3(0,0,90));
        play4_4.transform.Rotate(new Vector3(0,0,90));
    }
//Jugador 1--------------------------------------------------------
    public void Player1_1(){
        if (respuesta1_1==true)
        {
            correcta=true;
            TextCorrecto1.SetActive(true);
            TextIncorrecto1.SetActive(false);
        }
        else
        {
            correcta=false;
            TextCorrecto1.SetActive(false);
            TextIncorrecto1.SetActive(true);
        }
    }
    public void Player1_2(){
        if (respuesta1_2==true)
        {
            correcta=true;
            TextCorrecto1.SetActive(true);
            TextIncorrecto1.SetActive(false);
        }
        else
        {
            correcta=false;
            TextCorrecto1.SetActive(false);
            TextIncorrecto1.SetActive(true);
        }
    }
    public void Player1_3(){
        if (respuesta1_3==true)
        {
            correcta=true;
            TextCorrecto1.SetActive(true);
            TextIncorrecto1.SetActive(false);
        }
        else
        {
            correcta=false;
            TextCorrecto1.SetActive(false);
            TextIncorrecto1.SetActive(true);
        }
    }
    public void Player1_4(){
        if (respuesta1_4==true)
        {
            correcta=true;
            TextCorrecto1.SetActive(true);
            TextIncorrecto1.SetActive(false);
        }
        else
        {
            correcta=false;
            TextCorrecto1.SetActive(false);
            TextIncorrecto1.SetActive(true);
        }
    }
    //Jugador 2
    public void Player2_1(){
        if (respuesta2_1==true)
        {
            correcta=true;
            TextCorrecto2.SetActive(true);
            TextIncorrecto2.SetActive(false);
        }
        else
        {
            correcta=false;
            TextCorrecto2.SetActive(false);
            TextIncorrecto2.SetActive(true);
        }
    }
    public void Player2_2(){
        if (respuesta2_2==true)
        {
            correcta=true;
            TextCorrecto2.SetActive(true);
            TextIncorrecto2.SetActive(false);
        }
        else
        {
            correcta=false;
            TextCorrecto2.SetActive(false);
            TextIncorrecto2.SetActive(true);;
        }
    }
    public void Player2_3(){
        if (respuesta2_3==true)
        {
            correcta=true;
            TextCorrecto2.SetActive(true);
            TextIncorrecto2.SetActive(false);
        }
        else
        {
            correcta=false;
            TextCorrecto2.SetActive(false);
            TextIncorrecto2.SetActive(true);
        }
    }
    public void Player2_4(){
        if (respuesta2_4==true)
        {
            correcta=true;
            TextCorrecto2.SetActive(true);
            TextIncorrecto2.SetActive(false);
        }
        else
        {
            correcta=false;
            TextCorrecto2.SetActive(false);
            TextIncorrecto2.SetActive(true);
        }
    }
    //Jugador 3
    public void Player3_1(){
        if (respuesta3_1==true)
        {
            correcta=true;
            TextCorrecto3.SetActive(true);
            TextIncorrecto3.SetActive(false);
        }
        else
        {
            correcta=false;
            TextCorrecto3.SetActive(false);
            TextIncorrecto3.SetActive(true);
        }
    }
    public void Player3_2(){
        if (respuesta3_2==true)
        {
            correcta=true;
            TextCorrecto3.SetActive(true);
            TextIncorrecto3.SetActive(false);
        }
        else
        {
            correcta=false;
            TextCorrecto3.SetActive(false);
            TextIncorrecto3.SetActive(true);
        }
    }
    public void Player3_3(){
        if (respuesta3_3==true)
        {
            correcta=true;
            TextCorrecto3.SetActive(true);
            TextIncorrecto3.SetActive(false);
        }
        else
        {
            correcta=false;
            TextCorrecto3.SetActive(false);
            TextIncorrecto3.SetActive(true);
        }
    }
    public void Player3_4(){
        if (respuesta3_4==true)
        {
            correcta=true;
            TextCorrecto3.SetActive(true);
            TextIncorrecto3.SetActive(false);
        }
        else
        {
            correcta=false;
            TextCorrecto3.SetActive(false);
            TextIncorrecto3.SetActive(true);
        }
    }
    //Jugador 4
    public void Player4_1(){
        if (respuesta4_1==true)
        {
            correcta=true;
            TextCorrecto4.SetActive(true);
            TextIncorrecto4.SetActive(false);
        }
        else
        {
            correcta=false;
            TextCorrecto4.SetActive(false);
            TextIncorrecto4.SetActive(true);
        }
    }
    public void Player4_2(){
        if (respuesta4_2==true)
        {
            correcta=true;
            TextCorrecto4.SetActive(true);
            TextIncorrecto4.SetActive(false);
        }
        else
        {
            correcta=false;
            TextCorrecto4.SetActive(false);
            TextIncorrecto4.SetActive(true);
        }
    }
    public void Player4_3(){
        if (respuesta4_3==true)
        {
            correcta=true;
            TextCorrecto4.SetActive(true);
            TextIncorrecto4.SetActive(false);
        }
        else
        {
            correcta=false;
            TextCorrecto4.SetActive(false);
            TextIncorrecto4.SetActive(true);
        }
    }
    public void Player4_4(){
        if (respuesta4_4==true)
        {
            correcta=true;
            TextCorrecto4.SetActive(true);
            TextIncorrecto4.SetActive(false);
        }
        else
        {
            correcta=false;
            TextCorrecto4.SetActive(false);
            TextIncorrecto4.SetActive(true);
        }
    }
    

    // Start is called before the first frame update
    void Start()
    { 
        jugadores=1;
//respuesta jugador 1  
    respuesta1_1=false;
    respuesta1_2=false;
    respuesta1_3=false;
    respuesta1_4=false;
//respueta jugador 2
    respuesta2_1=false;
    respuesta2_2=false;
    respuesta2_3=false;
    respuesta2_4=false;
//respueta jugador 3    
    respuesta3_1=false;
    respuesta3_2=false;
    respuesta3_3=false;
    respuesta3_4=false;
//respuesta jugador 4
    respuesta4_1=false;
    respuesta4_2=false;
    respuesta4_3=false;
    respuesta4_4=false;
        play1_1 = Instantiate (Correctas1[0]) as GameObject;
        play1_2 = Instantiate (Correctas1[0]) as GameObject;
        play1_3 = Instantiate (Correctas1[0]) as GameObject;
        play1_4 = Instantiate (Correctas1[0]) as GameObject;

        play2_1 = Instantiate (Correctas1[0]) as GameObject;
        play2_2 = Instantiate (Correctas1[0]) as GameObject;
        play2_3 = Instantiate (Correctas1[0]) as GameObject;
        play2_4 = Instantiate (Correctas1[0]) as GameObject;

        play3_1 = Instantiate (Correctas1[0]) as GameObject;
        play3_2 = Instantiate (Correctas1[0]) as GameObject;
        play3_3 = Instantiate (Correctas1[0]) as GameObject;
        play3_4 = Instantiate (Correctas1[0]) as GameObject;

        play4_1 = Instantiate (Correctas1[0]) as GameObject;
        play4_2 = Instantiate (Correctas1[0]) as GameObject;
        play4_3 = Instantiate (Correctas1[0]) as GameObject;
        play4_4 = Instantiate (Correctas1[0]) as GameObject;

        play1_1.SetActive(false);
        play1_2.SetActive(false);
        play1_3.SetActive(false);
        play1_4.SetActive(false);

        play2_1.SetActive(false);
        play2_2.SetActive(false);
        play2_3.SetActive(false);
        play2_4.SetActive(false);

        play3_1.SetActive(false);
        play3_2.SetActive(false);
        play3_3.SetActive(false);
        play3_4.SetActive(false);

        play4_1.SetActive(false);
        play4_2.SetActive(false);
        play4_3.SetActive(false);
        play4_4.SetActive(false);
        
        Panel.SetActive(false);
        TextCorrecto1.SetActive(false);
        TextIncorrecto1.SetActive(false);
        TextCorrecto2.SetActive(false);
        TextIncorrecto2.SetActive(false);
        TextCorrecto3.SetActive(false);
        TextIncorrecto3.SetActive(false);
        TextCorrecto4.SetActive(false);
        TextIncorrecto4.SetActive(false);

        Baraja.SetActive(true);

        
    }

    // Update is called once per frame
    void Update()
    {
        
        //Repartir carta----------------------------------------------------------
        //Jugador 1
        //speed=20;
        float step = speed*Time.deltaTime;
        play1_1.transform.position = Vector3.MoveTowards(play1_1.transform.position, target_J1_1.position, step);
        play1_2.transform.position = Vector3.MoveTowards(play1_2.transform.position, target_J1_2.position, step);
        play1_3.transform.position = Vector3.MoveTowards(play1_3.transform.position, target_J1_3.position, step);
        play1_4.transform.position = Vector3.MoveTowards(play1_4.transform.position, target_J1_4.position, step);
        //Jugador 2

        play2_1.transform.position = Vector3.MoveTowards(play2_1.transform.position, target_J2_1.position, step);
    
        play2_2.transform.position = Vector3.MoveTowards(play2_2.transform.position, target_J2_2.position, step);
    
        play2_3.transform.position = Vector3.MoveTowards(play2_3.transform.position, target_J2_3.position, step);
    
        play2_4.transform.position = Vector3.MoveTowards(play2_4.transform.position, target_J2_4.position, step);
        //Jugador 3
        play3_1.transform.position = Vector3.MoveTowards(play3_1.transform.position, target_J3_1.position, step);
    
        play3_2.transform.position = Vector3.MoveTowards(play3_2.transform.position, target_J3_2.position, step);
    
        play3_3.transform.position = Vector3.MoveTowards(play3_3.transform.position, target_J3_3.position, step);
    
        play3_4.transform.position = Vector3.MoveTowards(play3_4.transform.position, target_J3_4.position, step);
        //Jugador 4

        play4_1.transform.position = Vector3.MoveTowards(play4_1.transform.position, target_J4_1.position, step);
    
        play4_2.transform.position = Vector3.MoveTowards(play4_2.transform.position, target_J4_2.position, step);
    
        play4_3.transform.position = Vector3.MoveTowards(play4_3.transform.position, target_J4_3.position, step);
    
        play4_4.transform.position = Vector3.MoveTowards(play4_4.transform.position, target_J4_4.position, step);
        //if general para dejar speed en 0

        if (play1_1.transform.position==target_J1_1.position && play1_2.transform.position==target_J1_2.position && play1_3.transform.position==target_J1_3.position && play1_4.transform.position==target_J1_4.position && play2_1.transform.position==target_J2_1.position && play2_2.transform.position==target_J2_2.position&& play2_3.transform.position==target_J2_3.position&& play2_4.transform.position==target_J2_4.position && play3_1.transform.position==target_J3_1.position&& play3_2.transform.position==target_J3_2.position&& play3_3.transform.position==target_J3_3.position&& play3_4.transform.position==target_J3_4.position&& play4_1.transform.position==target_J4_1.position&& play4_2.transform.position==target_J4_2.position&& play4_3.transform.position==target_J4_3.position&& play4_4.transform.position==target_J4_4.position)
        {
        speed=0;
        }
    }

}
