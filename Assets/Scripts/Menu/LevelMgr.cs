﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class LevelMgr : MonoBehaviour
{
    // Start is called before the first frame update
    //public Conexiones conexiones;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void CargaNivel(string pnombreNivel){
        SceneManager.LoadScene (pnombreNivel);
        if (pnombreNivel == "LoadingCreacion")
        {
            print("Crear");
            //conexiones.PlayTimeCreacion();
            GameObject.Find("Conexiones").GetComponent<Conexiones>().PlayTimeCreacion();
        }
        else if (pnombreNivel == "LoadingLaberinto")
        {
            print("Labe");
            GameObject.Find("Conexiones").GetComponent<Conexiones>().PlayTimeLaberinto();
        }
        else if (pnombreNivel == "LoadingAlternativas")
        {
            print("Alternativa");
            GameObject.Find("Conexiones").GetComponent<Conexiones>().PlayTimeAlternativa();
        }
        else if (pnombreNivel == "LoadingBusca")
        {
            print("Busca");
            GameObject.Find("Conexiones").GetComponent<Conexiones>().PlayTimeBusca();
        }
        else if (pnombreNivel == "LoadingDefensa")
        {
            print("Cuida");
            GameObject.Find("Conexiones").GetComponent<Conexiones>().PlayTimeCuida();
        }
        else if (pnombreNivel == "LoadingPuzzle")
        {
            print("Puzzle");
            GameObject.Find("Conexiones").GetComponent<Conexiones>().PlayTimePuzzle();
        }
        else if (pnombreNivel == "LoadingMenuVolver")
        {
            GameObject.Find("Conexiones").GetComponent<Conexiones>().FTimeVolver();
            print("Volver");
        }
        else if (pnombreNivel == "LoadingMenu")
        {
            GameObject.Find("Conexiones").GetComponent<Conexiones>().FTimeRecompensa();
            print("Recompensa");
        }
           
    }
}
