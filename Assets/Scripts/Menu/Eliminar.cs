﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Eliminar : MonoBehaviour
{
private int cont=0;
[SerializeField]private GameObject boton;
    public void MostrarEliminarObjeto(){
        if (Input.touches.Length<=0)
        {
            //nada
        }
        else{
            cont++;
            if (cont%2==1)
            {
                boton.SetActive(true);
            }
            else{
                boton.SetActive(false);
            }
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        boton.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
