﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class Barraproceso : MonoBehaviour
{
    public Transform BarraEspera;
    public Transform TextProceso;
    public Transform TextoCargando;
    [SerializeField] private float currentAmount;
    [SerializeField] private float speed;
    // Update is called once per frame
    void Update()
    {
        if(currentAmount<100){
            currentAmount += speed * Time.deltaTime;
            TextProceso.GetComponent<Text>().text =((int)currentAmount).ToString()+"%";
            TextoCargando.gameObject.SetActive(true);
        }
        else{
            TextoCargando.gameObject.SetActive(false);
            TextProceso.GetComponent<Text>().text="Listo";
            SceneManager.LoadScene("Menu");
        }

    }
}
