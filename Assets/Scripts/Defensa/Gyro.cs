﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gyro : MonoBehaviour
{
    private Gyroscope jyro;
    private void Start()
    {
        UnityEngine.Gyroscope jyro = Input.gyro;
        jyro.enabled = true;
    }

    void Update()
    {
        if (SystemInfo.supportsGyroscope)
        {
            transform.rotation = new Quaternion(Input.gyro.attitude.x,-Input.gyro.attitude.y,0,-Input.gyro.attitude.w);
        }
    }
}
