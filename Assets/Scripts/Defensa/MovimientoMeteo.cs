﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoMeteo : MonoBehaviour
{
    public CrearMeteoritos crear;
    public float speed = 1f;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.down * Time.deltaTime * speed);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "asteroidA" || other.gameObject.tag == "asteroidB" || other.gameObject.tag == "asteroidC" || other.gameObject.tag == "asteroidD" || other.gameObject.tag == "asteroidE" || other.gameObject.tag == "asteroidF" || other.gameObject.tag == "asteroidG" || other.gameObject.tag == "asteroidH")
        {
            Destroy(other.gameObject);
            print("a");

        }
    }
}
