﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cronometro : MonoBehaviour
{
    public Text UIText;
    public float time;
    public GameObject Texto;
    public GameObject finish;
    public CrearMeteoritos crearMeteoritos;
    public GameObject puntero;



    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Texto.SetActive(true);
        //time -= Time.deltaTime;
        if(time > 0)
        {
           
            time -= Time.deltaTime;
            
            //print(time);
            
        }
        else
        {   

            Finish();
            puntero.SetActive(false);
        }
        UIText.text = time.ToString("f0");
        
    }
    public void Play()
    {
        //time = 90;
        Update();
    }
    public void Finish()
    {
        finish.SetActive(true);
        
        crearMeteoritos.CancelInvoke();
    }
    
}
