﻿using MySql.Data.MySqlClient;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;

public class Raycast : MonoBehaviour
{
    public GameObject asteroidA;
    public GameObject asteroidB;
    public GameObject asteroidC;
    public GameObject asteroidD;
    public GameObject asteroidE;
    public GameObject asteroidF;
    public GameObject asteroidG;
    public GameObject asteroidH;
    public Text scoreText;
    public Transform camara;
    public float range;
    public int score;
    public Conexiones Conexiones;
    
    

    void Start()
    {
        score = 0;
        SetCountText();
    }
    void Update()
    {


        if (Physics.Raycast(camara.position, camara.forward, out RaycastHit hit, range))
        {
            //Debug.Log(hit.collider.name);
            if (hit.collider.gameObject.tag == "asteroidA" || hit.collider.gameObject.tag == "asteroidB" || hit.collider.gameObject.tag == "asteroidC" || hit.collider.gameObject.tag == "asteroidD" || hit.collider.gameObject.tag == "asteroidE" || hit.collider.gameObject.tag == "asteroidF" || hit.collider.gameObject.tag == "asteroidG" || hit.collider.gameObject.tag == "asteroidH")
            {
                //Debug.Log(hit.collider.name);
                Destroy(hit.collider.gameObject);
                score = score + 1;
                SetCountText();
                //print(hit.collider.gameObject.name);
                //cnn.Open();
                Conexiones.Eliminar();
                //GameObject.Find("Conexiones").GetComponent<Conexiones>().ColisionTCorrecto(SceneManager.GetActiveScene().name, hit.collider.gameObject.name);
            }
        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(camara.position, camara.forward * range);
    }

    public void SetCountText ()
    {
        scoreText.text = "Puntacion " + score.ToString();
    }
}

