﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class ShakeDemo : MonoBehaviour {
	
	private ShakePlugin shakePlugin;
	public Text shakeCountText;
	public string pasosS;
	public int pasos=0;
	public Alternativas alternativas;
    public Conexiones conexiones;

	public int pasos1;
	
	/* 
	
	public int velocidad=345;
	public int refresco=104;
	public Text shakeSpeedText;

	public Text sensitivityText;
	public Slider sensitivitySlider;

	public Text delayUpdateText;
	public Slider delayUpdateSlider;*/

	
	// Use this for initialization
	void Start (){
		// don't allow the device to sleep
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		
		shakePlugin = ShakePlugin.GetInstance();
		shakePlugin.SetDebug(0);
		shakePlugin.Init();
		SetSensitivitySlider();
		SetDelayUpdateSlider();

		shakePlugin.SetCallbackListener(OnShake);
		shakePlugin.RegisterSensorListener(SensorDelay.SENSOR_DELAY_NORMAL);
	}

	private void SetSensitivitySlider(){
		//int sensitivity = (int)sensitivitySlider.value;
		int sensitivity=2000;
		UpdateSensitivity(sensitivity);
	//	UpdateSensitivity(sensitilidad);
		Debug.Log("CheckSensitivitySlider value: " + sensitivity);
		//Debug.Log("CheckSensitivitySlider value: " + sensibilidad);
	} 

	private void SetDelayUpdateSlider(){
		//int delayUpdate = (int)delayUpdateSlider.value;
		int delayUpdate = 150;
		UpdateDelayUpdate(delayUpdate);
		Debug.Log("CheckDelayUpdateSlider value: " + delayUpdate);
		//UpdateDelayUpdate(refresco);
		//Debug.Log("CheckDelayUpdateSlider value: " + refresco);
	}

	public void OnSensitivitySliderChange(){
		SetSensitivitySlider();
	}
  
	public void OnDelayUpdateSliderChange(){
		SetDelayUpdateSlider();
	}

	private void OnApplicationPause(bool val){
		if(val){
			if(shakePlugin!=null){
				shakePlugin.RemoveSensorListener();
			}
		}else{
			if(shakePlugin!=null){
				shakePlugin.RegisterSensorListener(SensorDelay.SENSOR_DELAY_NORMAL);
			}
		}
	}

	private void OnShake(int count, float speed){
		UpdateShakeCount(count);
		//UpdateShakeSpeed(speed);
		//UpdateShakeCount(pasos);
		//UpdateShakeSpeed(velocidad);
	}

	private void UpdateSensitivity(int sensitivity){
		if(shakePlugin!=null){
			//sensitivity=velocidad;
			shakePlugin.SetSensitivity(sensitivity);
			//sensitivity=645;
			 /* if(sensitivityText!=null){
				sensitivityText.text = String.Format("Sensitivity: {0}",sensitivity);
			}*/
		}
	}

	private void UpdateDelayUpdate(int delayUpdate){
		if(shakePlugin!=null){
			//delayUpdate=refresco;
			shakePlugin.SetDelayUpdate(delayUpdate);
			//delayUpdate=104;
			
			/* if(delayUpdateText!=null){
				delayUpdateText.text = String.Format("Delay Update: {0}",delayUpdate);
			}*/
		}
	}

	public void UpdateShakeCount(int count){
		if(shakeCountText!=null){
			shakeCountText.text =String.Format("Saltos Realizados: {0}",count);
			pasos++;
			pasos1=count;
            if (pasos != 0 && count != 0)
            {
                conexiones.marcaPasos();
            }
			//pasosS="Pasos Realizados: "+shakeCountText.text;
		}
	}
/* 
	private void UpdateShakeSpeed(float speed){
		if(shakeCountText!=null){
			shakeSpeedText.text = String.Format("Shake Speed: {0}",speed);
		}
	}*/

	public void ResetShakeCount(){
		if(shakePlugin!=null){
			shakePlugin.ResetShakeCount();
			UpdateShakeCount(0);
			pasos=0;
		}
	}
}