﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class loadingtext : MonoBehaviour {

    private RectTransform rectComponent;
    private Image imageComp;

    public float speed = 400f;
    public Text text;
    public Text textNormal;

    public string nombre_escena;


    // Use this for initialization
    void Start () {
        rectComponent = GetComponent<RectTransform>();
        imageComp = rectComponent.GetComponent<Image>();
        imageComp.fillAmount = 0.0f;
    }
	
	// Update is called once per frame
	void Update () {
        int a = 0;
        if (imageComp.fillAmount != 1f)
        {
            imageComp.fillAmount = imageComp.fillAmount + Time.deltaTime * speed;
            a = (int)(imageComp.fillAmount * 100);
            if (a > 0 && a <= 67)
            {
                textNormal.text = "Cargando...";
            }
            else if (a > 67 && a <= 100)
            {
                textNormal.text = "Falta poco...";
            }
            else {

            }
            text.text = a + "%";
        }
        else
        {
            SceneManager.LoadScene(nombre_escena);
            //imageComp.fillAmount = 0.0f;
            //text.text = "0%";
        }
    }
}